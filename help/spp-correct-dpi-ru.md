﻿# ScanPostProcess. Коррекция DPI



[TOC]





## Введение

Этот алгоритм помогает получить аккуратно выровненные сканы, если у исходников плавающий DPI, как например, у фотографий. Работает в связке с проектом **[ScanTailor Advanced](https://github.com/4lex4/scantailor-advanced)**.





## Постановка задачи

Рассмотрим следующую ситуацию. Есть фото бумажной книги, сделанные камерой. Нужно сделать качественную растровую электронную книгу в **DjVu** или **PDF**.

Чтобы лучше понимать происходящее, разберем тестовый пример из 6 сканов-фотографий, сделанных с рук, без штатива. Тестовый проект можно найти в каталоге [example](../example) в [репозитории](https://gitlab.com/riva-lab/ScanPostProcess) проекта.





## Обычная обработка

Первое и абсолютно необходимое условие — предобработка сканов, в данном случае фотографий, в дальнейшем будем их называть просто сканами. Воспользуемся приложением **ScanTailor Advanced**. При загрузке сканов в проект, **ScanTailor Advanced** в таких случаях обычно требует установить правильное разрешение сканов.

![Окно Исправить DPI](screenshots/ex-dpi/s1.png)

Выбираем `300 DPI`, нажимаем кнопку `Применить`, затем `ОК`. Если это окно не появлялось и проект загрузился без явного задания DPI, выбираем меню `Инструменты > Исправить DPI`. Откроется то же самое окно. Теперь можно вручную задать DPI: вкладка `Все страницы`, дальше действуем как описано выше.

После загрузки сканов обрабатываем их как обычно. Так как наши сканы это фотографии, могут возникнуть специфические трудности. Например, из-за неравномерного освещения могут быть ошибки на 2-м этапе (разрезка страниц) или на 4-м (полезная область). Придется тщательно пересмотреть все сканы и вручную подкорректировать недочеты.

На этапе вывода ставим выходное разрешение небольшим (например, 300 DPI, на слабых машинах можно и меньше). Настраиваем при необходимости остальные параметры вывода. Выпрямление строк (dewarping) включаем только в том случае, если это необходимо для больше чем половины сканов. После чего запускаем пакетную обработку.

После окончания вывода всех сканов пересматриваем их. Включаем (или выключаем) выпрямление строк (dewarping), где необходимо. При этом не ждем вывода каждого скана: установили настройки и сразу переходим к следующему скану. После переходим на первый скан и снова запускаем пакетную обработку. После снова пересматриваем вывод и, если необходимо, вносим правки: например, вручную настраиваем выпрямление строк, если автоматический алгоритм не справился. В общем, добиваемся приемлемого качества сканов.

Завершив работу над проектом, сохраняем его и закрываем.





## Проблема фотографий

Однако существует одна изначальная проблема у всех сканов-фотографий: обычно они не имеют правильного разрешения в DPI. Это и понятно: расстояние от страницы книги до камеры постоянно, хоть и незначительно, меняется. Даже если съемка ведется не с рук, а со штатива — первая страница будет ближе к камере чем последняя на расстояние, равное толщине книги, иногда значительное. Во всех подобных случаях снимки с камеры имеют плавающий DPI.

Это приводит к вот такому эффекту:

![Эффект неравномерных полей](screenshots/ex-dpi/s8+.png)

В собранной из таких сканов э-книге страницы будут иметь разную ширину полей и, соответственно, контента. Это выглядит неопрятно. Особенно некрасиво выглядит э-книга, если сканы были изначально сняты камерой с рук. В этом случае ширина полей "скачет" от страницы к странице. Съемка со штатива этот эффект не устраняет, а скорее своеобразно "упорядочивает".





## Определяем настоящее разрешение

Чтобы решить эту небольшую неприятность с плавающими полями надо всего-то определить настоящее разрешение каждого скана и задать его в проекте **ScanTailor Advanced**.

Это можно сделать приложением **ScanPostProcess**. Запускаем, выбираем вкладку `Коррекция DPI`.

![ScanPostProcess - вкладка Коррекция DPI](screenshots/ex-dpi/s2.png)

Первым делом укажем с чем работать — перетаскиваем каталог с выходными обработанными сканами **ScanTailor Advanced** прямо в окно **ScanPostProcess**. Строка пути `Файл (или каталог)` станет зеленой, а кнопка `Пуск` активной. Но прежде чем ее нажимать, надо задать несколько параметров.

Самый важный параметр `Размер контента` — это настоящая ширина контента (или высота, в зависимости от выбранного направления расчета). Ее можно просто измерить линейкой. В данном случае имеем ширину 97 мм:

![Измерение ширины контента линейкой](screenshots/ex-dpi/measure.jpg)

В принципе, можно не заморачиваться так сильно с линейкой. Можно задать и приблизительное значение. В таком случае мы немного ошибемся количественно (DPI будет иметь такое же процентное отклонение от истинного, какое и указанная ширина), но на качество это не повлияет.

Следующий параметр `Коэффициент увеличения` — корректирующий множитель для случая, если в проекте ScanTailor Advanced для исходных сканов было задано значение DPI X, а при выводе было указано DPI Y; тогда коэффициент можно рассчитать как `X / Y`. Например, если при создании проекта указали значение 300 DPI, а выводили в 200 DPI, коэффициент будет `300 / 200 = 1,5`.

Следующий параметр — `Допуск значений DPI` в процентах. Если определенное значение DPI скана будет отличаться от среднего больше, чем задано, то оно будет заменено на среднее. Значение 0 отключает это поведение. Чтобы полностью контролировать рассчитанные DPI, можно установить 0.

Далее зададим направление, по которому будет идти расчет DPI. По `горизонтали` — размер заполненной области это ширина контента, по `вертикали` — высота.

Обратите внимание! Алгоритм расчета DPI определяет как контент области скана, которые темнее цвета `0xСССССС`. Поэтому поля изображений должны быть светлыми (цвет светлее `0xСССССС`), иначе рассчитанные значения будут некорректны!

Запускаем обработку кнопкой `Пуск`. Дожидаемся окончания процесса и переходим на вкладку `Стек`. Здесь видим все наши сканы. Сейчас нас интересуют только столбцы `Файл` и `DPI`. По правому щелчку на таблице можем отсортировать строки в удобном порядке. Эта вкладка служит для контроля и выявления аномальных значений DPI.

![ScanPostProcess - вкладка Стек](screenshots/ex-dpi/s3.png)





## Корректируем DPI

Возвращаемся обратно на вкладку `Коррекция DPI`. Перетаскиваем в окно файл нашего проекта **ScanTailor Advanced**. Не забудьте перед этим сохранить проект в **ScanTailor Advanced**!

Выбираем, хотим ли сохранять резервную копию файла проекта. При коррекции DPI исходный проект **ScanTailor Advanced** не изменяется, он только читается. Но перестраховаться будет нелишне, особенно если было много ручной работы над проектом — обидно ведь будет начинать все сначала.

![ScanPostProcess - вкладка Коррекция DPI](screenshots/ex-dpi/s4.png)

Жмем кнопку `Внедрить` и готово! В каталоге рядом с исходным проектом создается новый, название которого отличается от исходного суффиксом `-dpiCorr`. Обратите внимание, что рядом с исходным проектом появляется еще один файл с расширением `.backup` — резервная копия исходного проекта.

![ScanPostProcess - Файлы после коррекции](screenshots/ex-dpi/s5.png)





## Вывод скорректированных сканов

Возвращаемся в **ScanTailor Advanced**. Закрываем исходный проект, если он еще открыт. Теперь жмем `Открыть проект …` и выбираем наш новый скорректированный проект и открываем его. Можем убедиться в том, что сканам назначены новые значения DPI, открыв окно `Исправить DPI` и выбрав любой интересующий нас скан.

![ScanPostProcess - окно Исправить DPI](screenshots/ex-dpi/s6.png)

Переходим на последний этап — вывод. Ранее мы задавали выходное разрешение равное 300 DPI. Этого было вполне достаточно для определения DPI приложением **ScanPostProcess**. Но обычно 300 DPI недостаточно для качественного отображения страницы в э-книге. Поэтому зададим разрешение 600 DPI для всех страниц. Теперь осталось только запустить пакетную обработку и дождаться ее окончания.

![ScanPostProcess - окно Исправить DPI](screenshots/ex-dpi/s7.png)

На этом работу в **ScanTailor Advanced** можно считать завершенной. Теперь осталось собрать э-книгу в требуемом формате.





## Результат

Посмотрим, что дает использование **ScanPostProcess** для дополнительной обработки сканов. Закодируем страницы в **DjVu** и откроем э-книгу:

![ScanPostProcess - э-книга после исправления DPI](screenshots/ex-dpi/s9.png)

А вот что мы имели бы без коррекции DPI:

![ScanPostProcess - э-книга до исправления DPI](screenshots/ex-dpi/s8.png)

Обратите внимание на ширину полей. Именно ее выравнивания мы и добивались.





## Возможные проблемы и ограничения

Алгоритм определения DPI довольно простой — сканирование линий (в рассматриваемом случае — вертикальных). Если линия полностью пустая — не содержит ни одного темного пикселя — значит это поле. Как только в линии появится хоть один темный пиксель, считаем, что дальше начинается контент. Делаем так для левого поля, начиная сканирование с левой стороны. Потом по аналогии для правого поля. Зная координаты левой и правой границы полей (или контента, что то же самое), рассчитываем DPI: ширину в дюймах делим на ширину контента в пикселях.

Поэтому становится очевидным, что если на странице будет некий элемент, выступающий за основное поле (визуальное), то DPI будет посчитан неправильно. Этого можно избежать, удалив такие элементы из сканов. К примеру, можно сделать так:

1. Открываем каталог вывода **ScanTailor Advanced** в просмотрщике изображений, возьмем **[FastStone Image Viewer](https://www.faststone.org)**.
2. Открываем первый скан.
3. Если на скане есть выступающие за поля элементы — выделяем их и удаляем.
4. Не забываем сохранять изменения!
5. Переходим к следующему скану и пункту 3.

