unit u_image_dims;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazUTF8, fpeMetadata, u_image_metadata, u_utilities;

type

  { TImageDimensionData }

  TImageDimensionData = class

  private const
    DEFAULT_DPI       = 120;

  private
    FWidth:  Integer;
    FHeight: Integer;
    FxDPI:   Integer;
    FyDPI:   Integer;

    procedure GetTIFFSizesAndDPI(AStream: TStream; var w, h, xdpi, ydpi: Integer);

  public
    constructor Create(AFilename: String);
    destructor Destroy; override;

    property ImgWidth: Integer read FWidth;
    property ImgHeight: Integer read FHeight;
    property xDPI: Integer read FxDPI;
    property yDPI: Integer read FyDPI;
  end;

implementation


{ TImageDimensionData }

procedure TImageDimensionData.GetTIFFSizesAndDPI(AStream: TStream; var w, h, xdpi,
  ydpi: Integer);
  var
    metadata: TImgInfo;
  begin
    metadata := TImgInfo.Create;

      try
      AStream.Position := 0;
      metadata.LoadFromStream(AStream);
      if metadata.HasExif then
        with metadata.ExifData do
          begin
            try
            w := TagByName['ImageWidth'].AsInteger;
            h := TagByName['ImageHeight'].AsInteger;
            except
            w := TagByName['ExifImageWidth'].AsInteger;
            h := TagByName['ExifImageHeight'].AsInteger;
            end;
          xdpi := TagByName['XResolution'].AsInteger;
          ydpi := TagByName['YResolution'].AsInteger;
          if TagByName['ResolutionUnit'].AsInteger = 3 then
            begin
            xdpi := round(xdpi * 2.54);
            ydpi := round(ydpi * 2.54);
            end;
          end;
      except
      end;

    FreeAndNil(metadata);
  end;

constructor TImageDimensionData.Create(AFilename: String);
  var
    w, h:   word;
    stream: TFileStream;
  begin
    stream := TFileStream.Create(AFilename, fmOpenRead);

    case UTF8LowerCase(ExtractFileExt(AFilename)) of
      '.png':
        begin
        GetSizesPNG(stream, FWidth, FHeight);
        GetDPI_PNG(stream, FxDPI, FyDPI);
        end;

      '.bmp':
        begin
        GetSizes_BMP(stream, FWidth, FHeight);
        GetDpi_BMP(stream, FxDPI, FyDPI);
        end;

      '.jpg', '.jpeg':
        begin
        GetDpi_JPG(stream, FxDPI, FyDPI);
        if not InRange(FxDPI, 1, 10000) then
            try
            GetTIFFSizesAndDPI(stream, FWidth, FHeight, FxDPI, FyDPI);
            finally
            end;

        GetSizes_JPG(stream, FWidth, FHeight);
        end;

      '.tif', '.tiff':
        GetTIFFSizesAndDPI(stream, FWidth, FHeight, FxDPI, FyDPI);
      end;

    if not InRange(FxDPI, 1, 10000) then FxDPI := DEFAULT_DPI;
    if not InRange(FyDPI, 1, 10000) then FyDPI := DEFAULT_DPI;

    FreeAndNil(stream);
    inherited Create;
  end;

destructor TImageDimensionData.Destroy;
  begin
    inherited Destroy;
  end;

end.
