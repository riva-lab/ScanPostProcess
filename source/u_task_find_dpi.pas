unit u_task_find_dpi;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, u_image_process;

type

  { TTaskFindDPI }

  TTaskFindDPI = class
  private
    FDPI:        Integer;
    FSize:       Double;
    FCoeff:      Double;
    FHorizontal: Boolean;

  public
    constructor Create;
    destructor Destroy;

    procedure SetParameters(ASize, ACoeff: Double; AHorizontal: Boolean);
    procedure DoIt;

  public
    FileName: String;

  public
    property DPI: Integer read FDPI;
  end;


implementation

{ TTaskFindDPI }

constructor TTaskFindDPI.Create;
  begin
    Filename := '';
    inherited Create;
  end;

destructor TTaskFindDPI.Destroy;
  begin
    inherited Destroy;
  end;

procedure TTaskFindDPI.SetParameters(ASize, ACoeff: Double; AHorizontal: Boolean);
  begin
    FSize       := ASize;
    FCoeff      := ACoeff;
    FHorizontal := AHorizontal;
  end;

procedure TTaskFindDPI.DoIt;
  begin
    FDPI := GetDPI(Filename, FHorizontal, FSize, FCoeff);
  end;

end.
