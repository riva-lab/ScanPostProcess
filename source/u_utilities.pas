unit u_utilities;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;


function CheckBoolean(ABool: Boolean; ValueTrue, ValueFalse: Variant): Variant;
function min(Value1, Value2: Variant): Variant;
function max(Value1, Value2: Variant): Variant;

// проверяет значение переменной на вхождение в диапазон
function InRange(AValue, AMin, AMax: Variant): Boolean;


implementation

function CheckBoolean(ABool: Boolean; ValueTrue, ValueFalse: Variant): Variant;
  begin
    if ABool then
      Result := ValueTrue else
      Result := ValueFalse;
  end;

function min(Value1, Value2: Variant): Variant;
  begin
    if Value1 < Value2 then
      Result := Value1 else
      Result := Value2;
  end;

function max(Value1, Value2: Variant): Variant;
  begin
    if Value1 > Value2 then
      Result := Value1 else
      Result := Value2;
  end;

// проверяет значение переменной на вхождение в диапазон
function InRange(AValue, AMin, AMax: Variant): Boolean;
  begin
    Result := (AValue >= AMin) and (AValue <= AMax);
  end;

end.
