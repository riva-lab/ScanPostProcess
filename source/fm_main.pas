unit fm_main;

{$mode objfpc}{$H+}

interface

uses
  ActnList, Buttons, Classes, Clipbrd, ComCtrls, Controls, Dialogs, ExtCtrls,
  FileUtil, Forms, Graphics, Grids, IniPropStorage, Laz2_DOM, laz2_XMLRead,
  laz2_XMLWrite, LazFileUtils, LazUTF8, Translations, LCLIntf, LCLType, Menus,
  Spin, StdCtrls, LCLTranslator, StrUtils, SynEdit, SysUtils,
  win32taskbarprogress, u_about, u_image_dims, u_image_process,
  u_image_property, u_logger, u_task_content_align, u_utilities, u_worker,
  gettext;

resourcestring
  TXT_PROJFILE       = 'Файл проекта';
  TXT_IMAGES         = 'Изображений:';
  TXT_PROCNOTSTART   = 'Обработка не запущена';
  TXT_STOP           = 'Стоп';
  TXT_START          = 'Пуск';
  TXT_STACK_CLEARED  = 'Стек исходных файлов очищен!';
  TXT_TASK_DPI       = 'Задача: определение DPI.';
  TXT_TASK_ALIGN     = 'Задача: выравнивание контента.';
  TXT_PROC_START     = 'Обработка начата';
  TXT_PROC_FILE      = 'Начата обработка файлов [потоков: %d]';
  TXT_FILENAME       = 'Имя файла';
  TXT_TIME           = 'Время';
  TXT_FILENAME_OLD   = 'Старое имя';
  TXT_STATUS         = 'Статус';
  TXT_ERROR          = 'Ошибка';
  TXT_SUCCESS        = 'Успех';
  TXT_PROC_CANCELED  = 'Обработка прервана.';
  TXT_PROC_FINISHED  = 'Обработка завершена.';
  TXT_STAT_CANCELED  = 'Прервано';
  TXT_STAT_FINISHED  = 'Завершено';
  TXT_FILES_ENDED    = 'Обработано файлов: %0:d из %1:d.';
  TXT_FILES_STATUS   = 'Обработано: %0:d / %1:d%%';
  TXT_FILES_SELECTED = 'Файлы, выбранные для обработки:';
  TXT_FILES_ADDED    = 'Добавлено файлов: %0:d. Всего в стеке: %1:d.';
  TXT_DPI_CORRECT    = 'Коррекция отклонений DPI больше %d%%.';
  TXT_DPI_AVG        = 'Среднее значение DPI - %d.';
  TXT_DPI_ERROR      = 'Отклонение';
  TXT_DPI_CORRECTED  = 'Скорректировано значений: %d.';
  TXT_PROJ_DONE_RW   = 'Файл проекта STA <%s> успешно перезаписан.';
  TXT_PROJ_DONE_WR   = 'Файл проекта STA <%s> успешно записан.';
  TXT_PROJECT_ERROR  = 'При обработке файла проекта STA <%s> произошла ошибка.';


type

  { TfmMain }

  TfmMain = class(TForm)
    acClearFilesStack: TAction;
    acCorrect:        TAction;
    acGo:             TAction;
    acHelp:           TAction;
    acLogClear:       TAction;
    acLogCopy:        TAction;
    acLogToFile:      TAction;
    acShowLogOnGo:    TAction;
    acSortByDPICorrAsc: TAction;
    acSortByDPICorrDesc: TAction;
    acSortByNameAsc:  TAction;
    acSortByNameDesc: TAction;
    ActionList1:      TActionList;
    cbAlignSubDir:    TCheckBox;
    cbBackup:         TCheckBox;
    cbCustomDPI:      TCheckBox;
    cbFileFormat:     TCheckBox;
    cbFormat:         TComboBox;
    cbGrayscale:      TCheckBox;
    cbLogToFile:      TCheckBox;
    cbNormalize:      TCheckBox;
    cbRename:         TCheckBox;
    cbMultithreading: TCheckBox;
    cbShowLogOnGo:    TCheckBox;
    cbSplit:          TCheckBox;
    cbTranslate:      TCheckBox;
    edFile:           TEdit;
    edSTFile:         TEdit;
    im32a:            TImageList;
    im32d:            TImageList;
    IniPropStorage1:  TIniPropStorage;
    Label11:          TLabel;
    lbAAlign:         TLabel;
    lbAbout:          TLabel;
    lbAHeight:        TLabel;
    lbAHint:          TLabel;
    lbAlignEven:      TLabel;
    lbAlignOdd:       TLabel;
    lbAOffHorz:       TLabel;
    lbAOffVert:       TLabel;
    lbAppRepo:        TLabel;
    lbAWidth:         TLabel;
    lbDPICoeff:       TLabel;
    lbDPIDir:         TLabel;
    lbDPIHint:        TLabel;
    lbDPIRange:       TLabel;
    lbDPISize:        TLabel;
    lbDPIStep1:       TLabel;
    lbDPIStep2:       TLabel;
    lbFile:           TLabel;
    lbStatusDPI:      TLabel;
    lbStatusProj:     TLabel;
    lbSTFile:         TLabel;
    mAbout:           TMemo;
    MenuItem1:        TMenuItem;
    MenuItem2:        TMenuItem;
    MenuItem3:        TMenuItem;
    MenuItem4:        TMenuItem;
    MenuItem5:        TMenuItem;
    MenuItem6:        TMenuItem;
    MenuItem7:        TMenuItem;
    pAlign:           TPanel;
    pAlignRBtns1:     TPanel;
    pAlignRBtns2:     TPanel;
    Panel1:           TPanel;
    Panel10:          TPanel;
    Panel12:          TPanel;
    Panel2:           TPanel;
    Panel3:           TPanel;
    Panel5:           TPanel;
    Panel6:           TPanel;
    Panel7:           TPanel;
    Panel8:           TPanel;
    Panel9:           TPanel;
    pbProgress:       TProgressBar;
    pcPageCtrl:       TPageControl;
    pDPIDirection:    TPanel;
    pmLog:            TPopupMenu;
    pmStack:          TPopupMenu;
    pSettings:        TPanel;
    pTaskStatusDPI:   TPanel;
    pTaskStatusProj:  TPanel;
    pToolBar:         TPanel;
    pToolButtons:     TPanel;
    rbAlignBC1:       TRadioButton;
    rbAlignBC2:       TRadioButton;
    rbAlignBL1:       TRadioButton;
    rbAlignBL2:       TRadioButton;
    rbAlignBR1:       TRadioButton;
    rbAlignBR2:       TRadioButton;
    rbAlignCC1:       TRadioButton;
    rbAlignCC2:       TRadioButton;
    rbAlignCL1:       TRadioButton;
    rbAlignCL2:       TRadioButton;
    rbAlignCR1:       TRadioButton;
    rbAlignCR2:       TRadioButton;
    rbAlignTC1:       TRadioButton;
    rbAlignTC2:       TRadioButton;
    rbAlignTL1:       TRadioButton;
    rbAlignTL2:       TRadioButton;
    rbAlignTR1:       TRadioButton;
    rbAlignTR2:       TRadioButton;
    rbDPIDirHorz:     TRadioButton;
    rbDPIDirVert:     TRadioButton;
    sbDPICorr:        TSpeedButton;
    sbDPIGo:          TSpeedButton;
    sbHelp:           TSpeedButton;
    sbStatus:         TStatusBar;
    seCoeff:          TFloatSpinEdit;
    seCustomDPI:      TSpinEdit;
    seOffsetHorz:     TFloatSpinEdit;
    seOffsetVert:     TFloatSpinEdit;
    Separator1:       TMenuItem;
    Separator2:       TMenuItem;
    seRange:          TSpinEdit;
    seRenamePad:      TSpinEdit;
    seRenameStart:    TSpinEdit;
    seSize:           TFloatSpinEdit;
    seSizeH:          TFloatSpinEdit;
    seSizeW:          TFloatSpinEdit;
    sgStack:          TStringGrid;
    synLog:           TSynEdit;
    tbtnClearFilesStack: TToolButton;
    tbtnGo:           TToolButton;
    tmrUpdate:        TTimer;
    ToolBar1:         TToolBar;
    tsContentAlign:   TTabSheet;
    tsCorrectDPI:     TTabSheet;
    tsImagesStack:    TTabSheet;
    tsLog:            TTabSheet;
    tsSettings:       TTabSheet;
    acMultithreading: TAction;
    acTranslate:      TAction;

    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure FormCreate(Sender: TObject);
    procedure tmrUpdateTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acGoExecute(Sender: TObject);
    procedure acCorrectExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure acClearFilesStackExecute(Sender: TObject);
    procedure rbAlignChange(Sender: TObject);
    procedure cbRenameChange(Sender: TObject);
    procedure sgStackDblClick(Sender: TObject);
    procedure acActionExecute(Sender: TObject);
  private
    procedure GetTaskType;
    function GetScanTailorFileName(AFileNames: TStringList; var AFileName: String): Boolean;
    procedure CorrectScanTailorFile;
    procedure AddGraphicFiles(AFileNames: TStringList);
    function GetFileNumber(AIndex: Integer; AForce: Boolean = False): String;
    procedure UpdateImagesStringGrid;
    procedure StartProcessFiles;
    procedure SetupTaskCorrectDPI(worker: TWorkingThread);
    procedure SetupTaskContentAlign(worker: TWorkingThread);
    procedure StopProcessFiles;
    procedure OnThreadComplete(id: Integer);
    procedure CorrectInvalidDPIs;
    procedure TerminateAllWorkingThreads;
    procedure ShowTimeInStatusBar;
  end;


const
  LANGUAGES_DIR = DirectorySeparator + 'lang';

var
  fmMain:          TfmMain;
  isFile:          Boolean = False;
  isDir:           Boolean = False;
  stFile:          String = '';
  workers:         array of TWorkingThread;
  fileIndex:       Integer = 0;
  maxThreads:      Integer = 0;
  workingThreads:  Integer = 0;
  finishedTasks:   Integer = 0;
  isRunning:       Boolean = False;
  criticalSection: TCriticalSection;
  timeStart:       TTime;
  taskType:        TTaskType;
  log:             TCustomLogger;

implementation

{$R *.lfm}

{ TfmMain }

procedure TfmMain.FormDropFiles(Sender: TObject; const FileNames: array of String);
  var
    tmp: TStringList;
    i:   Integer;
  begin
    if Length(FileNames) <> 0 then
      begin
      isFile := FileExistsUTF8(FileNames[0]);
      isDir  := DirectoryExistsUTF8(FileNames[0]);

      tmp := TStringList.Create;
      if isFile then
        for i := 0 to High(FileNames) do
          tmp.Add(FileNames[i]);

      if isDir then
        tmp := FindAllFiles(FileNames[0], '', False);

      if tmp.Count > 0 then
        log.Filename := ExtractFilePath(tmp.Strings[0]) + 'spp-logfile.txt';

      if GetScanTailorFileName(tmp, stFile) then
        begin
        edSTFile.Text := stFile;

        if stFile <> '' then
          begin
          log.Add(TXT_PROJFILE + ' ScanTailor Advanced:');
          log.Add(stFile + LineEnding);
          end;
        end
      else
        begin
        AddGraphicFiles(tmp);
        edFile.Text := FileNames[0];
        end;

      FreeAndNil(tmp);
      end;

    isFile := isFile and (images.Count > 0);
    isDir  := isDir and (images.Count > 0);

    sbStatus.Panels.Items[0].Text := TXT_IMAGES + ' ' + images.Count.ToString;
    sbStatus.Panels.Items[1].Text := TXT_PROCNOTSTART;
  end;

procedure TfmMain.FormCreate(Sender: TObject);
  var
    i: Integer;
  begin
    log    := TCustomLogger.Create;
    images := TImagesPropertiesList.Create;
    InitializeCriticalSection(criticalSection);

    SetLength(workers, CPUCount);
    for i := 0 to High(workers) do
      workers[i] := TWorkingThread.Create(i);
  end;

procedure TfmMain.FormShow(Sender: TObject);
  var
    i: Integer;
  begin
    acActionExecute(acTranslate);

    if not FileExistsUTF8(ExtractFileNameWithoutExt(Application.ExeName) + '.ini') then
      for i := pcPageCtrl.PageCount - 1 downto 0 do
        begin
        pcPageCtrl.ActivePageIndex := i;
        AutoSize              := True;
        Position              := poScreenCenter;
        Constraints.MinHeight := Height;
        AutoSize              := False;
        end;

    fmMain.Caption            := GetAboutAppDescription;
    Application.Title         := GetAboutAppInternal;
    mAbout.BorderSpacing.Left := VertScrollBar.Size;
    lbAppRepo.Caption         := APP_SITE_ADDRESS;
    log.SynEdit               := synLog;
    sgStack.SelectedColor     := clGray;
    edFile.Caption            := '';
    edSTFile.Caption          := '';
    lbStatusDPI.Caption       := '';
    lbStatusProj.Caption      := '';
    tmrUpdate.Enabled         := True;
    GlobalTaskbarProgress     := TWin7TaskProgressBar.Create;

    FormDropFiles(Sender, []);
    rbAlignChange(Sender);
    acActionExecute(acLogToFile);
    UpdateImagesStringGrid;
  end;

procedure TfmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
  begin
    taskType := ttNone;
    StopProcessFiles;
    TerminateAllWorkingThreads;
    images.Destroy;
    Sleep(200);
  end;

procedure TfmMain.GetTaskType;
  begin
    case pcPageCtrl.ActivePage.Name of
      'tsCorrectDPI': taskType   := ttCorrectDPI;
      'tsContentAlign': taskType := ttContentAlign;
      end;
  end;

procedure TfmMain.tmrUpdateTimer(Sender: TObject);
  var
    i: Integer;
  begin
    isRunning   := False;
    for i       := 0 to maxThreads - 1 do
      isRunning := isRunning or workers[i].Running;

    BeginFormUpdate;

    acGo.Enabled                := (isFile or isDir);
    acClearFilesStack.Enabled   := not isRunning and (images.Count > 0);
    acMultithreading.Enabled    := not isRunning;
    acSortByDPICorrAsc.Enabled  := not isRunning;
    acSortByDPICorrDesc.Enabled := not isRunning;
    acSortByNameAsc.Enabled     := not isRunning;
    acSortByNameDesc.Enabled    := not isRunning;
    seSize.Enabled              := not isRunning;
    seCoeff.Enabled             := not isRunning;
    seRange.Enabled             := not isRunning;
    pDPIDirection.Enabled       := not isRunning;
    pSettings.Enabled           := not isRunning;
    cbBackup.Enabled            := not isRunning;
    pAlign.Enabled              := not isRunning;
    seSizeW.Enabled             := not isRunning;
    seSizeH.Enabled             := not isRunning;
    seOffsetHorz.Enabled        := not isRunning and (seOffsetHorz.Tag = 0);
    seOffsetVert.Enabled        := not isRunning and (seOffsetVert.Tag = 0);
    seCustomDPI.Enabled         := not isRunning and cbCustomDPI.Checked;
    cbCustomDPI.Enabled         := not isRunning;
    cbFormat.Enabled            := not isRunning and cbFileFormat.Checked;
    cbFileFormat.Enabled        := not isRunning;
    cbAlignSubDir.Enabled       := not isRunning;
    cbSplit.Enabled             := not isRunning;
    cbGrayscale.Enabled         := not isRunning;
    cbNormalize.Enabled         := not isRunning;
    cbRename.Enabled            := not isRunning;
    seRenameStart.Enabled       := not isRunning and cbRename.Checked;
    seRenamePad.Enabled         := not isRunning and cbRename.Checked;
    acCorrect.Enabled           := acGo.Enabled and not isRunning and (stFile <> '')
      and (fileIndex = images.Count);

    lbSTFile.Visible    := tsCorrectDPI.Showing;
    edSTFile.Visible    := tsCorrectDPI.Showing;
    pToolBar.Visible    := not (tsImagesStack.Showing or tsLog.Showing or tsSettings.Showing);
    edFile.Color        := CheckBoolean(isFile or isDir, $00B7FF6F, $00A6A6FF);
    edSTFile.Color      := CheckBoolean(stFile <> '', $00B7FF6F, $00A6A6FF);
    pbProgress.Position := finishedTasks;
    GlobalTaskbarProgress.Progress := finishedTasks;

    if isRunning then ShowTimeInStatusBar;
    acGo.Caption := CheckBoolean(isRunning, TXT_STOP, TXT_START);
    if isRunning then acGo.ImageIndex := 2 else acGo.ImageIndex := 1;

    EndFormUpdate;
  end;


procedure TfmMain.acGoExecute(Sender: TObject);
  begin
    if isRunning then
      StopProcessFiles
    else
      StartProcessFiles;
  end;

procedure TfmMain.acCorrectExecute(Sender: TObject);
  begin
    CorrectScanTailorFile;
  end;

procedure TfmMain.acClearFilesStackExecute(Sender: TObject);
  begin
    finishedTasks := 0;
    images.Create;
    FormDropFiles(Sender, []);
    log.Add(TXT_STACK_CLEARED + LineEnding);
    UpdateImagesStringGrid;
  end;


procedure TfmMain.rbAlignChange(Sender: TObject);

  function GetAssociatedTag(ASet: array of TRadioButton): Integer;
    var
      radioBtn: TRadioButton;
    begin
      for radioBtn in ASet do
        if radioBtn.Checked then Exit(radioBtn.Tag);
    end;

  function GetTagOfEnable(ASet: array of TRadioButton): Integer;
    var
      radioBtn: TRadioButton;
    begin
      Result := 0;
      for radioBtn in ASet do
        if radioBtn.Checked then Exit(1);
    end;

  begin
    pAlignRBtns1.Tag := GetAssociatedTag([
      rbAlignTL1, rbAlignTC1, rbAlignTR1,
      rbAlignCL1, rbAlignCC1, rbAlignCR1,
      rbAlignBL1, rbAlignBC1, rbAlignBR1]);

    pAlignRBtns2.Tag := GetAssociatedTag([
      rbAlignTL2, rbAlignTC2, rbAlignTR2,
      rbAlignCL2, rbAlignCC2, rbAlignCR2,
      rbAlignBL2, rbAlignBC2, rbAlignBR2]);

    seOffsetHorz.Tag :=
      GetTagOfEnable([rbAlignTC1, rbAlignCC1, rbAlignBC1]) *
      GetTagOfEnable([rbAlignTC2, rbAlignCC2, rbAlignBC2]);

    seOffsetVert.Tag :=
      GetTagOfEnable([rbAlignCL1, rbAlignCC1, rbAlignCR1]) *
      GetTagOfEnable([rbAlignCL2, rbAlignCC2, rbAlignCR2]);
  end;

procedure TfmMain.cbRenameChange(Sender: TObject);
  begin
    UpdateImagesStringGrid;
  end;

procedure TfmMain.sgStackDblClick(Sender: TObject);
  begin
    OpenDocument(images.FileNames[sgStack.Row - 1]);
  end;

procedure TfmMain.acActionExecute(Sender: TObject);
  begin
    case TComponent(Sender).Name of

      'acSortByNameAsc':
        images.SortByName(True);

      'acSortByNameDesc':
        images.SortByName(False);

      'acSortByDPICorrAsc':
        images.SortByDPICorrected(True);

      'acSortByDPICorrDesc':
        images.SortByDPICorrected(False);

      'acLogClear':
        synLog.ClearAll;

      'acLogToFile':
        log.WriteToFile := acLogToFile.Checked;

      'acTranslate':
        begin
        if acTranslate.Checked then SetDefaultLang('', LANGUAGES_DIR);
        mAbout.Text := GetAboutInfo;
        end;

      'acLogCopy':
        Clipboard.AsText := synLog.Text;

      'acHelp':
        OpenHelpFile;

      'lbAppRepo':
        OpenURL(APP_SITE_ADDRESS);

      end;

    if TComponent(Sender).Name.StartsWith('acSortBy') then
      UpdateImagesStringGrid;
  end;


procedure TfmMain.StartProcessFiles;
  var
    i: Integer;

  procedure addToLog;
    begin
      log.Add(DateTimeToStr(Now));
      log.AddFmt(TXT_PROC_FILE, [workingThreads]);

      case taskType of
        ttCorrectDPI:
          begin
          lbStatusDPI.Caption  := '';
          lbStatusProj.Caption := '';
          log.Add(TXT_TASK_DPI);
          log.AddFmt('DPI  | %-10s | %s', [TXT_FILENAME, TXT_TIME]);
          log.Add(AddChar('-', '', 28));
          end;

        ttContentAlign:
          begin
          log.Add(TXT_TASK_ALIGN);
          log.AddFmt('#%5s| %12s | %12s | %8s | %s', ['',
            TXT_FILENAME, TXT_FILENAME_OLD, TXT_TIME, TXT_STATUS]);
          log.Add(AddChar('-', '', 55));
          end;
        end;
    end;

  begin
    GetTaskType;
    if acShowLogOnGo.Checked then tsLog.Show;

    BeginFormUpdate;

    fileIndex                 := 0;
    finishedTasks             := 0;
    timeStart                 := Now;
    pbProgress.Max            := images.Count;
    pbProgress.Position       := fileIndex;
    GlobalTaskbarProgress.Max := images.Count;
    GlobalTaskbarProgress.Progress := fileIndex;

    maxThreads     := CheckBoolean(acMultithreading.Checked, Length(workers), 1);
    workingThreads := min(maxThreads, images.Count);

    for i := 0 to workingThreads - 1 do
      begin
      workers[i].TaskType   := taskType;
      workers[i].OnComplete := @OnThreadComplete;
      end;

    sbStatus.Panels.Items[1].Text := TXT_PROC_START;
    addToLog;

    for i := 0 to workingThreads - 1 do
      with workers[i] do
        begin
        case taskType of
          ttCorrectDPI: SetupTaskCorrectDPI(workers[i]);
          ttContentAlign: SetupTaskContentAlign(workers[i]);
          end;
        Index      := fileIndex;
        FileName   := images.FileNames[fileIndex];
        FileNumber := GetFileNumber(fileIndex);
        Inc(fileIndex);
        end;

    for i := 0 to workingThreads - 1 do
      workers[i].ContinueWork;

    EndFormUpdate;
  end;

procedure TfmMain.SetupTaskCorrectDPI(worker: TWorkingThread);
  begin
    worker.TaskCorrectDPI.SetParameters(
      seSize.Value, seCoeff.Value, rbDPIDirHorz.Checked);
  end;

procedure TfmMain.SetupTaskContentAlign(worker: TWorkingThread);
  var
    w, h, horz, vert: TImageSize;
    dpi:              Integer;
    outExt:           String;
  begin
    images.SelectByName(images.Names[fileIndex]);
    dpi := CheckBoolean(cbCustomDPI.Checked,
      seCustomDPI.Value, images.Selected.DPIOriginal);

    w    := TImageSize.Create(dpi);
    h    := TImageSize.Create(dpi);
    horz := TImageSize.Create(dpi);
    vert := TImageSize.Create(dpi);

    w.Millimeters    := seSizeW.Value;
    h.Millimeters    := seSizeH.Value;
    horz.Millimeters := seOffsetHorz.Value;
    vert.Millimeters := seOffsetVert.Value;

    with worker do
      begin
      outExt := UTF8LowerCase(cbFormat.Text);
      outExt := outExt.Remove(outExt.IndexOf(','));
      TaskContentAlign.OutputFormat := CheckBoolean(cbFileFormat.Checked, outExt, '');

      TaskContentAlign.SetParameters(
        w, h, horz, vert, TImageAlign(pAlignRBtns1.Tag), TImageAlign(pAlignRBtns2.Tag),
        CheckBoolean(cbAlignSubDir.Checked, 'outAlign', ''),
        cbSplit.Checked, cbGrayscale.Checked, cbNormalize.Checked);
      end;

    FreeAndNil(w);
    FreeAndNil(h);
    FreeAndNil(horz);
    FreeAndNil(vert);
  end;

procedure TfmMain.StopProcessFiles;
  var
    i: Integer;
  begin
    if Length(workers) > 0 then
      for i := 0 to workingThreads - 1 do
        workers[i].StopWork;

    if taskType = ttCorrectDPI then
      if (fileIndex > 0) and (fileIndex >= images.Count) and (seRange.Value > 0) then
        CorrectInvalidDPIs;
  end;

procedure TfmMain.OnThreadComplete(id: Integer);
  begin
    EnterCriticalSection(criticalSection);

    images.SelectByName(ExtractFileNameOnly(workers[id].FileNameOld));
    case taskType of

      ttCorrectDPI:
        begin
        images.Selected.DPINonCorrected := workers[id].TaskCorrectDPI.DPI;
        images.Selected.DPICorrected    := images.Selected.DPINonCorrected;
        log.AddFmt('%4d | %-10s | %s', [
          images.Selected.DPICorrected,
          images.Selected.Name,
          TimeToStr(Now)]);
        end;

      ttContentAlign:
        with workers[id] do
          log.AddFmt('#%4d | %12s | %12s | %s | %s', [
            images.IndexOfName(images.Selected.Name) + 1,
            ExtractFileName(FileName),
            ExtractFileName(FileNameOld),
            TimeToStr(Now),
            UnicodeLowerCase(CheckBoolean(TaskContentAlign.Error, TXT_ERROR, TXT_SUCCESS))]);
      end;

    if not workers[id].Stopped and (fileIndex < images.Count) then
      begin
      workingThreads         := maxThreads;
      workers[id].Index      := fileIndex;
      workers[id].FileName   := images.FileNames[fileIndex];
      workers[id].FileNumber := GetFileNumber(fileIndex);
      workers[id].ContinueWork;
      Inc(fileIndex);
      end
    else
      begin
      Dec(workingThreads);
      if workingThreads <= 0 then
        begin
        StopProcessFiles;
        log.Add(LineEnding + DateTimeToStr(Now));
        log.Add(CheckBoolean(fileIndex < images.Count, TXT_PROC_CANCELED, TXT_PROC_FINISHED));
        log.AddFmt(TXT_FILES_ENDED + LineEnding, [fileIndex, images.Count]);

        lbStatusDPI.Caption :=
          CheckBoolean(fileIndex < images.Count, TXT_STAT_CANCELED, TXT_STAT_FINISHED);
        end;
      end;

    finishedTasks                 := fileIndex - workingThreads;
    sbStatus.Panels.Items[1].Text := Format(TXT_FILES_STATUS,
      [finishedTasks, round(100 * finishedTasks / images.Count)]);

    LeaveCriticalSection(criticalSection);
    UpdateImagesStringGrid;
  end;

procedure TfmMain.TerminateAllWorkingThreads;
  var
    i: Integer;
  begin
    if Length(workers) > 0 then
      for i := 0 to maxThreads - 1 do
        begin
        workers[i].Terminate;
        WaitForThreadTerminate(workers[i].ThreadID, 1000);
        end;
  end;


procedure TfmMain.AddGraphicFiles(AFileNames: TStringList);
  const
    ALLOW_EXT: array of String = ('.png', '.tif', '.tiff', '.bmp', '.jpg', '.jpeg');
  var
    i:     Integer;
    added: Integer = 0;
  begin
    BeginFormUpdate;
    log.Add(DateTimeToStr(Now));

    if AFileNames <> nil then
      for i := 0 to AFileNames.Count - 1 do
        if UTF8LowerCase(ExtractFileExt(AFileNames.Strings[i])) in ALLOW_EXT then
          if images.AddFileName(AFileNames.Strings[i]) then
            with images.Selected do
              begin

              if added = 0 then
                log.Add(TXT_FILES_SELECTED);
              Inc(added);

              with TImageDimensionData.Create(FileName) do
                begin
                DPIOriginal   := xDPI;
                Width.Pixels  := ImgWidth;
                Height.Pixels := ImgHeight;
                Destroy;
                end;

              log.AddFmt('#%4d | %12s | %4d x %4d, %4d DPI, %7.1f x %7.1f mm', [
                images.Count, ExtractFileName(FileName),
                Width.Pixels, Height.Pixels, DPIOriginal,
                Width.Millimeters, Height.Millimeters]);
              end;

    log.AddFmt(TXT_FILES_ADDED + LineEnding, [added, images.Count]);

    EndFormUpdate;
    UpdateImagesStringGrid;
  end;

function TfmMain.GetFileNumber(AIndex: Integer; AForce: Boolean): String;
  begin
    if cbRename.Checked or AForce then
      Result := AddChar('0', IntToStr(seRenameStart.Value + AIndex),
        seRenamePad.Value)
    else
      Result := '';
  end;

procedure TfmMain.UpdateImagesStringGrid;
  var
    i, dpi: Integer;
  begin
    BeginFormUpdate;
    sgStack.RowCount := images.Count + 1;

    for i := 1 to images.Count do
      begin
      images.SelectByName(images.Names[i - 1]);
      with images.Selected do
        dpi := CheckBoolean(DPICorrected = 0, DPIOriginal, DPICorrected);

      sgStack.Cells[0, i] := i.ToString;
      sgStack.Cells[1, i] := images.Selected.Name;
      sgStack.Cells[2, i] := GetFileNumber(i - 1);
      sgStack.Cells[3, i] := images.Selected.FileID.ToString;
      sgStack.Cells[4, i] := dpi.ToString;
      end;

    EndFormUpdate;
  end;

function TfmMain.GetScanTailorFileName(AFileNames: TStringList; var AFileName: String): Boolean;
  const
    ALLOW_EXT = '.scantailor';
  var
    i: Integer;
  begin
    if AFileNames <> nil then
      for i := 0 to AFileNames.Count - 1 do
        if UTF8LowerCase(ExtractFileExt(AFileNames.Strings[i])) = ALLOW_EXT then
          begin
          AFileName := AFileNames.Strings[i];
          Exit(True);
          end;

    Result := False;
  end;

procedure TfmMain.CorrectInvalidDPIs;

  function GetAverageDPI: Double;
    var
      i: Integer;
    begin
      Result := 0;
      for i  := 0 to images.Count - 1 do
        begin
        images.SelectByName(images.Names[i]);
        Result += images.Selected.DPICorrected;
        end;
      if images.Count > 0 then
        Result /= images.Count;
    end;

  function DpiError(dpiOrig, dpiAvg: Integer): Double;
    begin
      Result := 100 * ((dpiOrig - dpiAvg) / dpiAvg);
    end;

  procedure ReportToLog(AverageValue: Integer);
    var
      i:   Integer;
      cnt: Integer = 0;
    begin
      for i := 0 to images.Count - 1 do
        with images.Selected do
          begin
          images.SelectByName(images.Names[i]);

          if IsDPICorrected then
            begin
            Inc(cnt);

            if cnt = 1 then
              begin
              log.AddFmt(LineEnding + TXT_DPI_CORRECT, [seRange.Value]);
              log.AddFmt(TXT_DPI_AVG, [AverageValue]);
              log.AddFmt('%-12s | %s', [TXT_DPI_ERROR, TXT_FILENAME]);
              log.Add(AddChar('-', '', 28));
              end;

            log.AddFmt('   %6.1f%%   | %s', [DpiError(DPINonCorrected, AverageValue), Name]);
            end;
          end;

      if cnt > 0 then
        log.AddFmt(TXT_DPI_CORRECTED, [cnt]);
    end;

  var
    i:          Integer;
    avg, level: Double;

  begin
    level := seRange.Value / 100;
    avg   := GetAverageDPI;

    if avg > 0 then
      for i := 0 to images.Count - 1 do
        with images.Selected do
          begin
          images.SelectByName(images.Names[i]);
          IsDPICorrected := abs(1 - DPINonCorrected / avg) > level;
          if IsDPICorrected then
            DPICorrected := round(avg);
          end;

    ReportToLog(round(avg));
  end;

procedure TfmMain.ShowTimeInStatusBar;
  const
    tmr: Integer         = 0;  // as static var
    strTimeToEnd: String = ''; // as static var
  var
    strOut:                String;
    timeToEnd, timePasted: TTime;
  begin
    // расчет прошедшего времени
    timePasted := Now - timeStart;
    DateTimeToString(strOut, 'nn:ss', timePasted, FormatSettings);

    // расчет оставшегося времени после 8 с работы
    if (finishedTasks > 0) and (SecsPerDay * timePasted > 8) then
      begin
      if tmr = 0 then
        begin
        tmr       := 10;
        timeToEnd := (images.Count - finishedTasks) * timePasted / finishedTasks;
        DateTimeToString(strTimeToEnd, 'nn:ss', timeToEnd, FormatSettings);
        end
      else
        tmr -= 1;

      strOut += ' / – ' + strTimeToEnd;
      end;

    sbStatus.Panels.Items[2].Text := strOut;
  end;

procedure TfmMain.CorrectScanTailorFile;
  var
    doc:              TXMLDocument;
    nFiles, nImages:  TDOMNodeList;
    attrName, attrId: TDOMNode;
    nodeDPI:          TDOMNode;
    attrH, attrV:     TDOMNode;
    i:                Integer;
    filename:         String;
    _fileExists:      Boolean;

  begin
    BeginFormUpdate;

    if cbBackup.Checked then
      CopyFile(stFile, stFile + '.backup');

    filename := ExtractFilePath(stFile) +
      ExtractFileNameOnly(stFile) + '-dpiCorr' + ExtractFileExt(stFile);

      try
      ReadXMLFile(doc, stFile);

      nFiles     := doc.DocumentElement.GetElementsByTagName('file');
      for i      := 0 to nFiles.Count - 1 do
        begin
        attrName := nFiles.Item[i].Attributes.GetNamedItem('name');
        attrId   := nFiles.Item[i].Attributes.GetNamedItem('id');
        if Assigned(attrName) and Assigned(attrId) then
          begin
          if images.SelectByName(ExtractFileNameOnly(attrName.NodeValue)) then
            images.Selected.FileID := attrId.NodeValue.ToInteger;
          end;
        end;

      nImages   := doc.DocumentElement.GetElementsByTagName('images').Item[0].GetChildNodes;
      for i     := 0 to nImages.Count - 1 do
        begin
        attrId  := nImages.Item[i].Attributes.GetNamedItem('fileId');
        nodeDPI := nImages.Item[i].FindNode('dpi');
        attrH   := nodeDPI.Attributes.GetNamedItem('horizontal');
        attrV   := nodeDPI.Attributes.GetNamedItem('vertical');

        if images.SelectByFileID(attrId.NodeValue.ToInteger) then
          begin
          attrH.NodeValue := images.Selected.DPICorrected.ToString;
          attrV.NodeValue := images.Selected.DPICorrected.ToString;
          //log.Add(images.Name + ' #' + images.ID.ToString + ' ' + images.DPI.ToString + ' DPI, ' +
          //  attrH.NodeValue + 'h/' + attrV.NodeValue + 'v');
          end;
          //else
          //log.Add('#' + attrId.NodeValue + ' ' +
          //  attrH.NodeValue + 'h/' + attrV.NodeValue + 'v');
        end;

      _fileExists := FileExistsUTF8(filename);
      WriteXMLFile(doc, filename);

      lbStatusProj.Caption := TXT_SUCCESS;

      log.AddFmt(CheckBoolean(_fileExists, TXT_PROJ_DONE_RW, TXT_PROJ_DONE_WR) + LineEnding,
        [ExtractFileNameOnly(filename)]);

      except
      lbStatusProj.Caption := TXT_ERROR;
      log.AddFmt(TXT_PROJECT_ERROR + LineEnding, [ExtractFileNameOnly(filename)]);
      end;

    UpdateImagesStringGrid;
    EndFormUpdate;
    FreeAndNil(doc);
  end;

end.
