unit u_image_process;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazUTF8, GdiPlus, LCLIntf, LCLType,
  u_utilities, FPReadTiff,
  BGRABitmap, BGRABitmapTypes, BGRAGraphics;

type
  TImageSide = (isLeft, isRight, isTop, isBottom);


function LoadImage(AFilename: String): TBGRABitmap;

function SaveImage(ABitmap: TBGRABitmap; AFilename: String; ADPI: Integer = 300): Boolean;

function GetImageMarginPosition(ABitmap: TBGRABitmap; ASide: TImageSide): Integer;

function GetDPI(Filename: String; IsHorizontal: Boolean; Size, Coeff: Double): Integer;

function SetImageNewSize(ABitmap: TBGRABitmap; SrcArea: TRect;
  NewW, NewH, OffsetX, OffsetY: Integer): TBGRABitmap;

// вовзращает флаг: true = источник был только 2-цветный (ч+б)
function GetBWContent(var ASource, ADest: TBGRABitmap; ARemoveFromSrc: Boolean): Boolean;


implementation

var
  criticalSection: TCriticalSection;


function LoadImage(AFilename: String): TBGRABitmap;
  var
    readerTiff: TFPReaderTiff;
  begin
    if AFilename = '' then Exit(nil);

    Result := TBGRABitmap.Create;

    // загрузка изображения
    case UTF8LowerCase(ExtractFileExt(AFilename)) of

      '.png', '.bmp', '.jpg', '.jpeg':
        Result.LoadFromFileUTF8(AFilename);

      '.tif', '.tiff':
          try
          readerTiff := TFPReaderTiff.Create;
          Result.LoadFromFileUTF8(AFilename, readerTiff);
          FreeAndNil(readerTiff);
          except
          FreeAndNil(readerTiff);
          FreeAndNil(Result);
          Exit(nil);
          end;

      else
        begin
        FreeAndNil(Result);
        Exit(nil);
        end;
      end;
  end;

function SaveImage(ABitmap: TBGRABitmap; AFilename: String; ADPI: Integer): Boolean;
  var
    gdiImage:    IGPBitmap;
    encParams:   IGPEncoderParameters;
    jpegQuality: Integer = 97 + 1;
  begin
    if (ABitmap = nil) or (AFilename = '') then Exit(False);

    EnterCriticalSection(criticalSection);
      try
        try
        encParams := TGPEncoderParameters.Create;
        encParams.Add(EncoderQuality, jpegQuality);
        gdiImage  := TGPBitmap.Create(ABitmap.Bitmap.Handle, HPALETTE(0));
        gdiImage.SetResolution(ADPI, ADPI);
        except
        Exit(False);
        end;
      finally
      LeaveCriticalSection(criticalSection);
      end;

    // сохранение изображения
      try
      case UTF8LowerCase(ExtractFileExt(AFilename)) of

        '.png':
          gdiImage.Save(UnicodeString(AFilename), TGPImageFormat.Png);

        '.jpg', '.jpeg':
          gdiImage.Save(UnicodeString(AFilename), TGPImageFormat.Jpeg, encParams);

        '.bmp':
          gdiImage.Save(UnicodeString(AFilename), TGPImageFormat.Bmp);

        '.tif', '.tiff':
          gdiImage.Save(UnicodeString(AFilename), TGPImageFormat.Tiff);
        end;
      except
      Result := False;
      end;

    Result := True;
    //if Result then Result := SetImageDPI(AFilename, ADPI);
  end;

function GetImageMarginPosition(ABitmap: TBGRABitmap; ASide: TImageSide): Integer;
  var
    x, y, w, h:         Integer;
    Side, IsHorizontal: Boolean;
  begin
    w            := ABitmap.Width - 1;
    h            := ABitmap.Height - 1;
    Side         := ASide in [isLeft, isTop];
    IsHorizontal := ASide in [isLeft, isRight];

    if Side then
      begin
      x := 0;
      y := 0;
      end
    else
      begin
      x := w;
      y := h;
      end;

    while True do
      begin
      if IsHorizontal then
        begin
        for y := 0 to h do
          if ABitmap.CanvasBGRA.Pixels[x, y] < $CCCCCC then
            Exit(x);

        if Side then
          if x < w then Inc(x) else Exit(-1)
        else
        if x > 0 then  Dec(x)  else Exit(-1);
        end
      else
        begin
        for x := 0 to w do
          if ABitmap.CanvasBGRA.Pixels[x, y] < $CCCCCC then
            Exit(y);

        if Side then
          if y < h then Inc(y) else Exit(-1)
        else
        if y > 0 then  Dec(y)  else Exit(-1);
        end;
      end;
  end;

function GetDPI(Filename: String; IsHorizontal: Boolean; Size, Coeff: Double): Integer;
  var
    d1, d2: Integer;
    image:  TBGRABitmap;
  begin
    image := LoadImage(Filename);
    if image = nil then Exit(0);

    // вычисление DPI
    d1     := GetImageMarginPosition(image, CheckBoolean(IsHorizontal, isLeft, isTop));
    d2     := GetImageMarginPosition(image, CheckBoolean(IsHorizontal, isRight, isBottom));
    Result := round(Abs(d1 - d2) / Size * 25.4 * Coeff);

    FreeAndNil(image);
  end;

function SetImageNewSize(ABitmap: TBGRABitmap; SrcArea: TRect; NewW, NewH,
  OffsetX, OffsetY: Integer): TBGRABitmap;
  var
    imagePart: TBGRABitmap;
  begin
    if ABitmap = nil then Exit(nil);

      try
      imagePart := ABitmap.GetPart(SrcArea);
      Result    := TBGRABitmap.Create(NewW, NewH, clWhite);
      Result.PutImage(OffsetX, OffsetY, imagePart, dmSet);
      FreeAndNil(imagePart);
      except
      Exit(nil);
      end;
  end;

function GetBWContent(var ASource, ADest: TBGRABitmap; ARemoveFromSrc: Boolean): Boolean;
  var
    x, y, w, h: Integer;
    pixel:      TColor;
  begin
    if ASource = nil then Exit(False);

    w      := ASource.Width;
    h      := ASource.Height;
    ADest  := TBGRABitmap.Create(w, h, clWhite);
    Result := True;

    Dec(w);
    Dec(h);
    for x := 0 to w do
      for y := 0 to h do
        begin
        pixel := ASource.CanvasBGRA.Pixels[x, y];
        if pixel = clBlack then
          begin
          ADest.CanvasBGRA.Pixels[x, y]     := clBlack;
          if ARemoveFromSrc then
            ASource.CanvasBGRA.Pixels[x, y] := clWhite;
          end
        else
        if pixel <> clWhite then
          Result := False;
        end;
  end;


initialization
  InitializeCriticalSection(criticalSection);

finalization
  DeleteCriticalSection(criticalSection);

end.
