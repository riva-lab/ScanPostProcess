unit u_image_property;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, u_utilities;

type

  { TImageSize }

  TImageSize = class
  private
    FPixels:      Integer;
    FMillimeters: Double;
    FDPI:         Integer;

    procedure SetPixels(AValue: Integer);
    procedure SetMillimeters(AValue: Double);
    procedure SetDPI(AValue: Integer);

  public
    constructor Create(ADPI: Integer = 300; APixels: Integer = 0);
    procedure Assign(Source: TImageSize);

    property Pixels: Integer read FPixels write SetPixels;
    property Millimeters: Double read FMillimeters write SetMillimeters;
    property DPI: Integer read FDPI write SetDPI;
  end;


  { TImageProperty }
  PImageProperty = ^TImageProperty;
  TImageProperty = class
  private
    FDPIOriginal: Integer;

    procedure SetDPIOriginal(AValue: Integer);

  public
    constructor Create;

  public
    FileName: String;
    Name:     String;
    FileID:   Integer;

    Width:           TImageSize;
    Height:          TImageSize;
    DPICorrected:    Integer;
    DPINonCorrected: Integer;
    IsDPICorrected:  Boolean;

    property DPIOriginal: Integer read FDPIOriginal write SetDPIOriginal;
  end;


  { TImagesPropertiesList }

  TImagesPropertiesList = class(TFPList)
  private
    FSelected: TImageProperty;
    FCurrent:  Integer;

    procedure Select(AIndex: Integer);
    function GetFileNames(Index: Integer): String;
    function GetNames(Index: Integer): String;

  public
    constructor Create;
    destructor Destroy;

    function AddFileName(AFileName: String): Boolean;
    function IndexOfFileID(AFileID: Integer): Integer;
    function IndexOfName(AName: String): Integer;

    function SelectByFileID(AFileID: Integer): Boolean;
    function SelectByName(AName: String): Boolean;

    procedure SortByName(AAscending: Boolean);
    procedure SortByDPICorrected(AAscending: Boolean);

  public
    property Selected: TImageProperty read FSelected;
    property Names[Index: Integer]: String read GetNames;
    property FileNames[Index: Integer]: String read GetFileNames;

  end;

var
  images: TImagesPropertiesList;


implementation


{ TImageSize }

procedure TImageSize.SetDPI(AValue: Integer);
  begin
    if FDPI = AValue then Exit;
    FDPI := AValue;
    SetPixels(FPixels);
  end;

procedure TImageSize.SetMillimeters(AValue: Double);
  begin
    FMillimeters := AValue;
    FPixels      := round(FMillimeters * FDPI / 25.4);
  end;

procedure TImageSize.SetPixels(AValue: Integer);
  begin
    FPixels        := AValue;
    if FDPI > 0 then
      FMillimeters := FPixels * 25.4 / FDPI;
  end;

constructor TImageSize.Create(ADPI: Integer; APixels: Integer);
  begin
    FDPI   := ADPI;
    Pixels := APixels;
  end;

procedure TImageSize.Assign(Source: TImageSize);
  begin
    FDPI         := Source.DPI;
    FPixels      := Source.Pixels;
    FMillimeters := Source.Millimeters;
  end;


{ TImageProperty }

procedure TImageProperty.SetDPIOriginal(AValue: Integer);
  begin
    FDPIOriginal := AValue;
    Width.DPI    := FDPIOriginal;
    Height.DPI   := FDPIOriginal;
  end;

constructor TImageProperty.Create;
  begin
    Width  := TImageSize.Create;
    Height := TImageSize.Create;
  end;


{ TImagesPropertiesList }

function CompareImagePropertyNameAsc(Item1, Item2: Pointer): Integer;
  begin
    if PImageProperty(Item1)^.Name = PImageProperty(Item2)^.Name then Exit(0);
    Result := CheckBoolean(PImageProperty(Item1)^.Name > PImageProperty(Item2)^.Name, 1, -1);
  end;

function CompareImagePropertyNameDesc(Item1, Item2: Pointer): Integer;
  begin
    if PImageProperty(Item1)^.Name = PImageProperty(Item2)^.Name then Exit(0);
    Result := CheckBoolean(PImageProperty(Item1)^.Name < PImageProperty(Item2)^.Name, 1, -1);
  end;

function CompareImagePropertyDPICorrectedAsc(Item1, Item2: Pointer): Integer;
  begin
    if PImageProperty(Item1)^.DPICorrected = PImageProperty(Item2)^.DPICorrected then Exit(0);
    Result := CheckBoolean(PImageProperty(Item1)^.DPICorrected > PImageProperty(Item2)^.DPICorrected, 1, -1);
  end;

function CompareImagePropertyDPICorrectedDesc(Item1, Item2: Pointer): Integer;
  begin
    if PImageProperty(Item1)^.DPICorrected = PImageProperty(Item2)^.DPICorrected then Exit(0);
    Result := CheckBoolean(PImageProperty(Item1)^.DPICorrected < PImageProperty(Item2)^.DPICorrected, 1, -1);
  end;

constructor TImagesPropertiesList.Create;
  begin
    inherited Create;
    Clear;
    FCurrent := -1;
  end;

destructor TImagesPropertiesList.Destroy;
  begin
    inherited Destroy;
  end;

procedure TImagesPropertiesList.Select(AIndex: Integer);
  begin
    FCurrent  := AIndex;
    FSelected := PImageProperty(Items[FCurrent])^;
  end;

function TImagesPropertiesList.GetFileNames(Index: Integer): String;
  begin
    if InRange(Index, 0, Count - 1) then
      Result := PImageProperty(Items[Index])^.FileName
    else
      Result := '';
  end;

function TImagesPropertiesList.GetNames(Index: Integer): String;
  begin
    if InRange(Index, 0, Count - 1) then
      Result := PImageProperty(Items[Index])^.Name
    else
      Result := '';
  end;


function TImagesPropertiesList.AddFileName(AFileName: String): Boolean;
  var
    p: PImageProperty;
  begin
    Result := IndexOfName(ExtractFileNameOnly(AFileName)) < 0;
    if not Result then Exit;

    Getmem(p, SizeOf(TImageProperty));
    p^          := TImageProperty.Create;
    p^.FileName := AFileName;
    p^.Name     := ExtractFileNameOnly(AFileName);
    Add(PImageProperty(p));

    Select(Count - 1);
  end;

function TImagesPropertiesList.IndexOfFileID(AFileID: Integer): Integer;
  var
    i: Integer;
  begin
    Result := -1;
    if Count > 0 then
      for i := 0 to Count - 1 do
        if PImageProperty(Items[i])^.FileID = AFileID then
          Exit(i);
  end;

function TImagesPropertiesList.IndexOfName(AName: String): Integer;
  var
    i: Integer;
  begin
    Result := -1;
    if Count > 0 then
      for i := 0 to Count - 1 do
        if PImageProperty(Items[i])^.Name = AName then
          Exit(i);
  end;

function TImagesPropertiesList.SelectByFileID(AFileID: Integer): Boolean;
  var
    i: Integer;
  begin
    i      := IndexOfFileID(AFileID);
    Result := i >= 0;
    if Result then
      Select(i);
  end;

function TImagesPropertiesList.SelectByName(AName: String): Boolean;
  var
    i: Integer;
  begin
    i      := IndexOfName(AName);
    Result := i >= 0;
    if Result then
      Select(i);
  end;

procedure TImagesPropertiesList.SortByName(AAscending: Boolean);
  begin
    if AAscending then
      Sort(@CompareImagePropertyNameAsc)
    else
      Sort(@CompareImagePropertyNameDesc);
  end;

procedure TImagesPropertiesList.SortByDPICorrected(AAscending: Boolean);
  begin
    if AAscending then
      Sort(@CompareImagePropertyDPICorrectedAsc)
    else
      Sort(@CompareImagePropertyDPICorrectedDesc);
  end;

end.
