unit u_task_content_align;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazUTF8, LazFileUtils,
  BGRABitmap, BGRAGraphics, BGRABitmapTypes,
  u_image_process, u_image_property, u_utilities, u_image_metadata;

type

  TImageAlign = (
    iaTopLeft = 0, iaTopCenter, iaTopRight,
    iaCenterLeft, iaCenterCenter, iaCenterRight,
    iaBottomLeft, iaBottomCenter, iaBottomRight);


  { TTaskContentAlign }

  TTaskContentAlign = class
  private
    FWidth, FHeight, FMarginHorz, FMarginVert: TImageSize;

    FAlign1, FAlign2: TImageAlign; // парный и непарный файл
    FError, FIsSplit: Boolean;
    FIsGrayscale:     Boolean;
    FIsNormalize:     Boolean;
    FSubDirectory:    String;

  public
    constructor Create;
    destructor Destroy;

    procedure SetParameters(AWidth, AHeight, AMarginHorz, AMarginVert: TImageSize;
      AAlign1, AAlign2: TImageAlign; ASubDirectory: String; AIsSplit,
      AIsGrayScale, AIsNormalize: Boolean);
    procedure DoIt;

  public
    Index:        Integer;
    FileName:     String;
    FileNumber:   String;
    NewFileName:  String;
    OutputFormat: String;

    property Error: Boolean read FError;
  end;


implementation

{ TTaskContentAlign }

constructor TTaskContentAlign.Create;
  begin
    Filename      := '';
    OutputFormat  := '';
    FSubDirectory := '';
    FWidth        := TImageSize.Create;
    FHeight       := TImageSize.Create;
    FMarginHorz   := TImageSize.Create;
    FMarginVert   := TImageSize.Create;
    FAlign1       := iaCenterCenter;
    FError        := False;
    FIsSplit      := False;
    FIsGrayscale  := False;
    FIsNormalize  := False;

    inherited Create;
  end;

destructor TTaskContentAlign.Destroy;
  begin
    FWidth.Destroy;
    FHeight.Destroy;
    FMarginHorz.Destroy;
    FMarginVert.Destroy;

    inherited Destroy;
  end;

procedure TTaskContentAlign.SetParameters(AWidth, AHeight, AMarginHorz,
  AMarginVert: TImageSize; AAlign1, AAlign2: TImageAlign; ASubDirectory: String;
  AIsSplit, AIsGrayScale, AIsNormalize: Boolean);
  begin
    FWidth.Assign(AWidth);
    FHeight.Assign(AHeight);
    FMarginHorz.Assign(AMarginHorz);
    FMarginVert.Assign(AMarginVert);

    FAlign1       := AAlign1;
    FAlign2       := AAlign2;
    FSubDirectory := ASubDirectory;
    FIsSplit      := AIsSplit;
    FIsGrayscale  := AIsGrayScale;
    FIsNormalize  := AIsNormalize;
  end;

procedure TTaskContentAlign.DoIt;
  var
    img, tmp:         TBGRABitmap;
    offsetX, offsetY: Integer;
    al, ar, at, ab:   Integer;
    step:             Integer = 0;
    isBW:             Boolean = False;
    _align:           TImageAlign;

  procedure GetMarginPositions;
    begin
      if not (_align in [iaBottomLeft, iaBottomCenter, iaBottomRight]) then
        at := GetImageMarginPosition(img, isTop)
      else
        at := 0;

      if not (_align in [iaTopLeft, iaTopCenter, iaTopRight]) then
        ab := GetImageMarginPosition(img, isBottom)
      else
        ab := img.Height - 1;

      if not (_align in [iaTopRight, iaCenterRight, iaBottomRight]) then
        al := GetImageMarginPosition(img, isLeft)
      else
        al := 0;

      if not (_align in [iaTopLeft, iaCenterLeft, iaBottomLeft]) then
        ar := GetImageMarginPosition(img, isRight)
      else
        ar := img.Width - 1;
    end;

  procedure GetOffsets;
    begin
      if _align in [iaCenterCenter, iaCenterLeft, iaCenterRight] then
        offsetY := (FHeight.Pixels - ab + at) div 2
      else
      if _align in [iaBottomLeft, iaBottomCenter, iaBottomRight] then
        offsetY := FHeight.Pixels - ab - FMarginVert.Pixels
      else
        offsetY := FMarginVert.Pixels;

      if _align in [iaCenterCenter, iaTopCenter, iaBottomCenter] then
        offsetX := (FWidth.Pixels - ar + al) div 2
      else
      if _align in [iaCenterRight, iaTopRight, iaBottomRight] then
        offsetX := FWidth.Pixels - ar - FMarginHorz.Pixels
      else
        offsetX := FMarginHorz.Pixels;
    end;

  function GetNewFilename(AIsFileBW: Boolean): String;
    begin
      Result := ExtractFileDir(FileName) + DirectorySeparator +
        CheckBoolean(FSubDirectory <> '', FSubDirectory + DirectorySeparator, '');

      if not DirectoryExistsUTF8(ExtractFileDir(Result)) then
        CreateDirUTF8(ExtractFileDir(Result));

      Result += CheckBoolean(FIsSplit and AIsFileBW, 'text' + DirectorySeparator, '') +
        CheckBoolean(FIsSplit and not AIsFileBW, 'back' + DirectorySeparator, '') +
        CheckBoolean(FileNumber = '', ExtractFileNameOnly(FileName), FileNumber) +
        CheckBoolean(OutputFormat = '', ExtractFileExt(FileName), '.' + OutputFormat);

      if not DirectoryExistsUTF8(ExtractFileDir(Result)) then
        CreateDirUTF8(ExtractFileDir(Result));
    end;

  function RedirectBitmap(AObjSrc: TBGRABitmap; var AObjFree: TBGRABitmap): TBGRABitmap;
    begin
      Result   := AObjSrc;
      AObjFree.Destroy;
      AObjFree := AObjSrc;
    end;

  begin
    FError := False;
    tmp    := nil;

    while not FError do
      begin
      Inc(step);

      case step of

        1: begin
          _align := CheckBoolean((Index and 1) = 0, FAlign1, FAlign2);
          img    := LoadImage(FileName);

          GetMarginPositions;
          GetOffsets;

          FError := RedirectBitmap(SetImageNewSize(img, Rect(al, at, ar, ab),
            FWidth.Pixels, FHeight.Pixels, offsetX, offsetY), img) = nil;
          end;

        2: if FIsSplit then
              try
              isBW        := GetBWContent(img, tmp, True);
              NewFileName := GetNewFilename(True);
              FError      := not SaveImage(tmp, NewFileName, FWidth.DPI);
              finally
              tmp.Destroy;
              end;

        3: if not isBW and FIsGrayscale then
              try
              FError := RedirectBitmap(img.FilterGrayscale, img) = nil;
              except
              FError := True;
              end;

        4: if not isBW and FIsNormalize then
              try
              FError := RedirectBitmap(img.FilterNormalize, img) = nil;
              except
              FError := True;
              end;

        5: if not isBW then
            begin
            NewFileName := GetNewFilename(False);
            FError      := not SaveImage(img, NewFileName, FWidth.DPI);
            end;

        6: break;

        end;
      end;

    img.Destroy;
  end;

end.
