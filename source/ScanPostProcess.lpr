program ScanPostProcess;

{$mode objfpc}{$H+}

uses
 {$IFDEF UNIX}
  cthreads,
   {$ENDIF}
 {$IFDEF HASAMIGA}
  athreads,
   {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, fm_main, u_image_property, u_worker, u_image_dims, u_image_metadata,
  u_image_process, u_task_content_align, u_task_find_dpi, u_utilities,
  u_fpwritepngcustom, win32taskbarprogress, u_about, u_appinfo, u_logger
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
