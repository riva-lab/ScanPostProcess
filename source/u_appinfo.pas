unit u_appinfo;


interface

uses
  SysUtils, Controls, LCLIntf, app_ver;

resourcestring
  ABOUT_VERSION     = 'версия';
  ABOUT_BUILD       = 'сборка';

const
  FILE_LICENSE      = 'license.md';
  FILE_LICENSE_HTML = 'license.html';
  FILE_README       = 'readme.md';
  FILE_README_HTML  = 'readme.html';

type

  { TAboutApp }

  TAboutApp = class
    procedure OpenLicense;
    procedure OpenReadme;

  private
    FAppArc:           String;
    FAppAuthor:        String;
    FAppBrief:         String;
    FAppBuild:         String;
    FAppCopyright:     String;
    FAppDescription:   String;
    FAppIntName:       String;
    FAppSite:          String;
    FAppSiteAvailable: Boolean;
    FAppVersion:       String;
    FAppComments:      String;
    FInfo:             String;

  public
    constructor Create;

    procedure UpdateInfo;

    property Info: String read FInfo;
    property AppIntName: String read FAppIntName;
    property AppVersion: String read FAppVersion;
    property AppBuild: String read FAppBuild;
    property AppArc: String read FAppArc;
    property AppAuthor: String read FAppAuthor;
    property AppCopyright: String read FAppCopyright;
    property AppDescription: String read FAppDescription;
    property AppBrief: String read FAppBrief;
    property AppSite: String read FAppSite;
    property AppSiteAvailable: Boolean read FAppSiteAvailable;
  end;

var
  fmAbout: TAboutApp;

implementation


{ TAboutApp }

procedure TAboutApp.OpenLicense;
  begin
    if not OpenDocument(FILE_LICENSE) then
      if not OpenDocument('..' + DirectorySeparator + FILE_LICENSE_HTML) then
        OpenDocument('..' + DirectorySeparator + FILE_LICENSE);
  end;

procedure TAboutApp.OpenReadme;
  begin
    if not OpenDocument(FILE_README) then
      if not OpenDocument('..' + DirectorySeparator + FILE_README_HTML) then
        OpenDocument('..' + DirectorySeparator + FILE_README);
  end;

constructor TAboutApp.Create;
  begin
    UpdateInfo;
  end;

procedure TAboutApp.UpdateInfo;
  const
  {$IfDef WIN64}
    sys_arc = 'win64';
  {$Else IfDef WIN32}
    sys_arc = 'win32';
  {$EndIf}
  var
    BuildDateTime: TDateTime;
  begin
    TryStrToDate({$INCLUDE %DATE%}, BuildDateTime, 'YYYY/MM/DD', '/');

    ReadAppInfo;
    FAppIntName     := app_info.InternalName;
    FAppArc         := sys_arc;
    FAppAuthor      := app_info.CompanyName;
    FAppBuild       := ABOUT_BUILD + ' ' + FormatDateTime('yyyy.mm.dd', BuildDateTime);
    FAppDescription := app_info.FileDescription;
    FAppVersion     := ABOUT_VERSION + ' ' + app_info.FileVersion;
    FAppCopyright   := '© ' + app_info.LegalCopyright;
    //FAppComments      := app_info.Comments;
    //FAppComments      := ABOUT_DESCR;
    FAppBrief       := FAppIntName + ' ' + FAppVersion + ', ' + FAppArc + ', ' + FAppBuild;

    FInfo := app_info.InternalName + LineEnding;
    FInfo += FAppVersion + ', ' + FAppArc + LineEnding;
    FInfo += FAppBuild + LineEnding + LineEnding;

    if FAppCopyright.Length > 0 then
      FInfo += FAppCopyright + LineEnding;

    if FAppAuthor.Length > 0 then
      FInfo += FAppAuthor + LineEnding;

    if FAppComments.Length > 0 then
      FInfo += FAppComments + LineEnding;
  end;

end.
