unit u_worker;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, u_task_find_dpi, u_task_content_align;

type

  TTaskType = (ttNone, ttCorrectDPI, ttContentAlign);

  TWorkerCallback = procedure(WorkerID: Integer) of object;

  { TWorkingThread }

  TWorkingThread = class(TThread)
  private
    FWorkerID: Integer;
    FRunning:  Boolean;

    procedure Execute; override;
    procedure OnBeginCallback;
    procedure OnCompleteCallback;

  public
    constructor Create(AWorkerID: Integer; ATaskType: TTaskType = ttCorrectDPI);
    destructor Destroy; override;
    procedure ContinueWork;
    procedure StopWork;

  public
    Stopped:          Boolean;
    Index:            Integer;
    FileName:         String;
    FileNameOld:      String;
    FileNumber:       String;
    TaskType:         TTaskType;
    OnComplete:       TWorkerCallback;
    OnBegin:          TWorkerCallback;
    TaskCorrectDPI:   TTaskFindDPI;
    TaskContentAlign: TTaskContentAlign;

    property Running: Boolean read FRunning;
  end;


implementation

{ TWorkingThread }

constructor TWorkingThread.Create(AWorkerID: Integer; ATaskType: TTaskType);
  begin
    FreeOnTerminate := False;
    FWorkerID       := AWorkerID;
    OnBegin         := nil;
    OnComplete      := nil;
    FRunning        := False;
    Stopped         := True;
    Filename        := '';

    TaskType         := ATaskType;
    TaskCorrectDPI   := TTaskFindDPI.Create;
    TaskContentAlign := TTaskContentAlign.Create;

    inherited Create(True);
  end;

destructor TWorkingThread.Destroy;
  begin
    TaskCorrectDPI.Destroy;
    TaskContentAlign.Destroy;

    inherited Destroy;
  end;

procedure TWorkingThread.ContinueWork;
  begin
    FRunning  := True;
    Stopped   := False;
    Suspended := False;
  end;

procedure TWorkingThread.StopWork;
  begin
    Stopped := True;
  end;

procedure TWorkingThread.Execute;
  begin
    while not Terminated do
      begin
      Suspended   := not FRunning or Stopped;
      Synchronize(@OnBeginCallback);
      FileNameOld := FileName;

      case TaskType of

        ttCorrectDPI:
          begin
          TaskCorrectDPI.FileName := FileName;
          TaskCorrectDPI.DoIt;
          end;

        ttContentAlign:
          begin
          TaskContentAlign.Index      := Index;
          TaskContentAlign.FileName   := FileName;
          TaskContentAlign.FileNumber := FileNumber;
          TaskContentAlign.DoIt;

          FileName := TaskContentAlign.NewFileName;
          end;
        end;

      FRunning := False;

      Synchronize(@OnCompleteCallback);
      end;

    Terminate;
  end;

procedure TWorkingThread.OnBeginCallback;
  begin
    if Assigned(OnBegin) then OnBegin(FWorkerID);
  end;

procedure TWorkingThread.OnCompleteCallback;
  begin
    if Assigned(OnComplete) then OnComplete(FWorkerID);
  end;


end.
