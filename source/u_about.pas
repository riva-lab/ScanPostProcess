unit u_about;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LCLIntf, LCLType, u_appinfo, gettext;

const
  FILE_HELP        = 'spp-help';
  APP_SITE_ADDRESS = 'https://gitlab.com/riva-lab/ScanPostProcess';



function GetAboutInfo: String;
function GetAboutAppDescription: String;
function GetAboutAppInternal: String;
function OpenHelpFile: Boolean;

implementation

function GetTextFromRes(AResourceName: String): String;
  var
    rs: TResourceStream;
    st: TStringList;
  begin
    rs     := TResourceStream.Create(HINSTANCE, AResourceName, RT_RCDATA);
    st     := TStringList.Create;
    st.LoadFromStream(rs);
    Result := st.Text;
  end;

function GetAboutInfo: String;
  var
    info: TAboutApp;
  begin
    info := TAboutApp.Create;

    Result := info.Info + LineEnding;
    Result += GetTextFromRes('about');

    FreeAndNil(info);
  end;

function GetAboutAppDescription: String;
  var
    info: TAboutApp;
  begin
    info   := TAboutApp.Create;
    Result := info.AppDescription;
    FreeAndNil(info);
  end;

function GetAboutAppInternal: String;
  var
    info: TAboutApp;
  begin
    info   := TAboutApp.Create;
    Result := info.AppIntName;
    FreeAndNil(info);
  end;

function OpenHelpFile: Boolean;
  var
    path1, path2, dummy, language: String;
  begin
    Result := False;
    path1  := 'help' + DirectorySeparator + FILE_HELP;
    path2  := '..' + DirectorySeparator + path1;
    GetLanguageIDs(dummy, language);

    if OpenDocument(Format('%s.%s', [path1, 'html'])) or
      OpenDocument(Format('%s.%s', [path1, 'md'])) or
      OpenDocument(Format('%s.%s', [path2, 'html'])) or
      OpenDocument(Format('%s.%s', [path2, 'md'])) or
      OpenDocument(Format('%s-%s.%s', [path1, language, 'html'])) or
      OpenDocument(Format('%s-%s.%s', [path1, language, 'md'])) or
      OpenDocument(Format('%s-%s.%s', [path2, language, 'html'])) or
      OpenDocument(Format('%s-%s.%s', [path2, language, 'md']))
    then
      Exit(True);
  end;

end.
