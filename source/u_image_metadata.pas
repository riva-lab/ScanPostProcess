{
  Функции для считывания размеров и разрешения графических форматов
  без полной загрузки файла.

  DPI   - https://forum.lazarus.freepascal.org/index.php?topic=40663.0
  Sizes - https://stackoverflow.com/questions/15209076/how-can-i-get-dimensions-of-an-image-file-in-delphi/15209265#15209265
}

unit u_image_metadata;

{$mode objfpc}{$H+}

interface

uses
  Classes, LazUTF8;

function SetDpi_BMP(AStream: TStream; dpiX, dpiY: Double): Boolean;
function GetDpi_BMP(AStream: TStream; out dpiX, dpiY: Integer): Boolean;
function GetSizes_BMP(AStream: TStream; out W, H: Integer): Boolean;

function SetDPI_TIF(AStream: TStream; dpiX, dpiY: Double): Boolean;
function GetDPI_TIF(AStream: TStream; out dpiX, dpiY: Integer): Boolean;

function SetDpi_JPG(AStream: TStream; dpiX, dpiY: Double): Boolean;
function GetDpi_JPG(AStream: TStream; out dpiX, dpiY: Integer): Boolean;
procedure GetSizes_JPG(AStream: TStream; out Width, Height: Integer);

function SetDpi_PNG(AStream: TStream; dpiX, dpiY: Double): Boolean;
function GetDPI_PNG(AStream: TStream; out dpiX, dpiY: Integer): Boolean;
procedure GetSizesPNG(AStream: TStream; out Width, Height: Integer);

function SetDpi_PCX(AStream: TStream; dpiX, dpiY: Double): Boolean;
function SetDPI_WMF(AStream: TStream; dpi: Double): Boolean;


function SetImageDPI(AFilename: String; ANewDPI: Integer): Boolean;

type
  TBitMapFileHeader = packed record
    bfType:     word;
    bfSize:     longint;
    bfReserved: longint;
    bfOffset:   longint;
  end;
  TBitMapInfoHeader = packed record
    Size:          longint;
    Width:         longint;
    Height:        longint;
    Planes:        word;
    BitCount:      word;
    Compression:   longint;
    SizeImage:     longint;
    XPelsPerMeter: Longint;
    YPelsPerMeter: Longint;
    ClrUsed:       longint;
    ClrImportant:  longint;
  end;

implementation

uses
  FpImgCmn, SysUtils, Math;

type
  TByteOrder = (boLE, boBE);  // little edian, or big endian

{ Makes sure that the byte order of w is as specified by the parameter. the
  function is for reading. }
function FixByteOrderR(w: Word; AByteOrder: TByteOrder): Word; overload;
  begin
    Result := IfThen(AByteOrder = boLE, LEToN(w), BEToN(w));
  end;

{ Makes sure that the byte order of dw is as specified by the parameter.
  The function is for reading. }
function FixByteOrderR(dw: DWord; AByteOrder: TByteOrder): DWord; overload;
  begin
    Result := IfThen(AByteOrder = boLE, LEToN(dw), BEToN(dw));
  end;

function FixByteOrderW(w: Word; AByteOrder: TByteOrder): Word; overload;
  begin
    Result := IfThen(AByteOrder = boLE, NToLE(w), NToBE(w));
  end;

function FixByteOrderW(dw: DWord; AByteOrder: TByteorder): DWord; overload;
  begin
    Result := IfThen(AByteOrder = boLE, NToLE(dw), NToBE(dw));
  end;


{ BMP files }

function SetDpi_BMP(AStream: TStream; dpiX, dpiY: Double): Boolean;
  const
    BMP_MAGIC_WORD = Ord('M') shl 8 or Ord('B');
  var
    header: TBitmapFileHeader;
    info:   TBitmapInfoHeader;
    p:      Int64;
  begin
    Result := False;
    if AStream.Read(header{%H-}, SizeOf(header)) <> SizeOf(header) then Exit;
    if LEToN(header.bfType) <> BMP_MAGIC_WORD then Exit;
    p := AStream.Position;
    if AStream.Read(info{%H-}, SizeOf(info)) <> SizeOf(info) then Exit;
    if info.Size < 40 then
      Exit;
    dpiX               := dpiX / 0.0254;  // convert to pixels per meter
    dpiY               := dpiY / 0.0254;
    info.XPelsPerMeter := NToLE(round(dpiX));
    info.YPelsPerMeter := NToLE(round(dpiY));
    AStream.Position   := p;
    AStream.WriteBuffer(info, Sizeof(info));
    AStream.Position   := 0;
    Result             := True;
  end;

function GetDpi_BMP(AStream: TStream; out dpiX, dpiY: Integer): Boolean;
  const
    BMP_MAGIC_WORD = Ord('M') shl 8 or Ord('B');
  var
    header: TBitmapFileHeader;
    info:   TBitmapInfoHeader;
    p:      Int64;
  begin
    Result := False;
    if AStream.Read(header{%H-}, SizeOf(header)) <> SizeOf(header) then Exit;
    if LEToN(header.bfType) <> BMP_MAGIC_WORD then Exit;
    p := AStream.Position;
    if AStream.Read(info{%H-}, SizeOf(info)) <> SizeOf(info) then Exit;
    if info.Size < 40 then
      Exit;
    AStream.Position := p;
    AStream.ReadBuffer(info, Sizeof(info));
    AStream.Position := 0;
    dpiX             := NToLE(info.XPelsPerMeter);
    dpiY             := NToLE(info.YPelsPerMeter);
    dpiX             := round(dpiX * 0.0254);  // convert to pixels per inch
    dpiY             := round(dpiY * 0.0254);
    Result           := True;
  end;

function GetSizes_BMP(AStream: TStream; out W, H: Integer): Boolean;
  const
    BMP_MAGIC_WORD = Ord('M') shl 8 or Ord('B');
  var
    header: TBitmapFileHeader;
    info:   TBitmapInfoHeader;
    p:      Int64;
  begin
    Result := False;
    if AStream.Read(header{%H-}, SizeOf(header)) <> SizeOf(header) then Exit;
    if LEToN(header.bfType) <> BMP_MAGIC_WORD then Exit;
    p := AStream.Position;
    if AStream.Read(info{%H-}, SizeOf(info)) <> SizeOf(info) then Exit;
    if info.Size < 40 then
      Exit;
    AStream.Position := p;
    AStream.ReadBuffer(info, Sizeof(info));
    AStream.Position := 0;
    H                := NToLE(info.Height);
    W                := NToLE(info.Width);
    Result           := True;
  end;


{ TIF }

function SetDPI_TIF(AStream: TStream; dpiX, dpiY: Double): Boolean;
  type
    TTifHeader = packed record
      BOM: word;     // 'II' for little endian, 'MM' for big endian
      Sig: word;     // Signature (42)
      IFD: DWORD;    // Offset where image data begin
      end;
    TIFD_Field = packed record
      Tag:       word;
      FieldType: word;
      ValCount:  DWord;
      ValOffset: DWord;
      end;
  var
    header:     TTifHeader = (BOM: 0; Sig: 0; IFD: 0);
    dirEntries: Word;
    field:      TIFD_Field = (Tag: 0; FieldType: 0; ValCount: 0; ValOffset: 0);
    p, pStart:  Int64;
    bo:         TByteOrder;
    i:          Integer;
    done:       Integer;
    num, denom: LongInt;
  begin
    Result           := False;
    AStream.Position := 0;
    pStart           := AStream.Position;
    if AStream.Read(header, SizeOf(TTifHeader)) < SizeOf(TTifHeader) then Exit;
    if header.BOM = $4949 then
      bo := boLE
    else if header.BOM = $4D4D then
      bo := boBE
    else
      Exit;
    if FixByteOrderR(header.Sig, bo) <> 42 then Exit;

    done             := 0;
    AStream.Position := pStart + FixByteOrderR(header.IFD, bo);
    dirEntries       := FixByteOrderR(AStream.ReadWord, bo);
    for i := 1 to dirEntries do
      begin
      AStream.Read(field, SizeOf(field));
      field.Tag       := FixByteOrderR(field.Tag, bo);
      field.ValOffset := FixByteOrderR(field.ValOffset, bo);
      field.FieldType := FixByteOrderR(field.FieldType, bo);
      p               := AStream.Position;
      case field.Tag of
        $011A: begin   // dpix as rational number
          AStream.Position := pStart + field.ValOffset;
          num              := Round(dpiX * 100);
          num              := FixByteOrderW(DWord(num), bo);
          denom            := 100;
          denom            := FixByteOrderW(DWord(denom), bo);
          AStream.WriteDWord(num);
          AStream.WriteDWord(denom);
          Inc(done);
          end;
        $011B: begin  // dpiy as rational number
          AStream.Position := pStart + field.ValOffset;
          num              := round(dpiY * 100);
          num              := FixByteOrderW(DWord(num), bo);
          denom            := 100;
          denom            := FixByteOrderW(DWord(denom), bo);
          AStream.WriteDWord(num);
          AStream.WriteDWord(denom);
          Inc(done);
          end;
        $0128: begin
          AStream.Position := p - SizeOf(field.ValOffset);
          AStream.WriteWord(FixByteOrderW(Word(2), bo));  // 2 = per inch
          Inc(done);
          Result           := True;
          end;
        end;
      if (done = 3) then break;
      AStream.Position := p;
      end;
    AStream.Position   := 0;
  end;

function GetDPI_TIF(AStream: TStream; out dpiX, dpiY: Integer): Boolean;
  type
    TTifHeader = packed record
      BOM: word;     // 'II' for little endian, 'MM' for big endian
      Sig: word;     // Signature (42)
      IFD: DWORD;    // Offset where image data begin
      end;
    TIFD_Field = packed record
      Tag:       word;
      FieldType: word;
      ValCount:  DWord;
      ValOffset: DWord;
      end;
  var
    header:     TTifHeader = (BOM: 0; Sig: 0; IFD: 0);
    dirEntries: Word;
    field:      TIFD_Field = (Tag: 0; FieldType: 0; ValCount: 0; ValOffset: 0);
    p, pStart:  Int64;
    bo:         TByteOrder;
    i:          Integer;
    done:       Integer;
    num, denom: LongInt;
  begin
    Result           := False;
    AStream.Position := 0;
    pStart           := AStream.Position;
    if AStream.Read(header, SizeOf(TTifHeader)) < SizeOf(TTifHeader) then Exit;
    if header.BOM = $4949 then
      bo := boLE
    else if header.BOM = $4D4D then
      bo := boBE
    else
      Exit;
    if FixByteOrderR(header.Sig, bo) <> 42 then Exit;

    done             := 0;
    AStream.Position := pStart + FixByteOrderR(header.IFD, bo);
    dirEntries       := FixByteOrderR(AStream.ReadWord, bo);
    for i := 1 to dirEntries do
      begin
      AStream.Read(field, SizeOf(field));
      field.Tag       := FixByteOrderR(field.Tag, bo);
      field.ValOffset := FixByteOrderR(field.ValOffset, bo);
      field.FieldType := FixByteOrderR(field.FieldType, bo);
      p               := AStream.Position;
      case field.Tag of
        $011A: begin   // dpix as rational number
          AStream.Position := pStart + field.ValOffset;
          num              := FixByteOrderW(DWord(AStream.ReadDWord), bo);
          denom            := FixByteOrderW(DWord(AStream.ReadDWord), bo);
          dpiX             := round(num / denom);
          Inc(done);
          end;
        $011B: begin  // dpiy as rational number
          AStream.Position := pStart + field.ValOffset;
          num              := FixByteOrderW(DWord(AStream.ReadDWord), bo);
          denom            := FixByteOrderW(DWord(AStream.ReadDWord), bo);
          dpiY             := round(num / denom);
          Inc(done);
          end;
        $0128: begin
          AStream.Position := p - SizeOf(field.ValOffset);
          if FixByteOrderW(AStream.ReadWord, bo) = 3 then  // 2 = inches, 3 = cm
            begin
            dpiX := round(dpiX * 2.54);
            dpiY := round(dpiY * 2.54);
            end;
          Inc(done);
          end;
        end;
      if (done = 3) then break;
      AStream.Position := p;
      end;
    AStream.Position   := 0;
    Result             := True;
  end;


{ JPG files }

function SetDpi_JPG(AStream: TStream; dpiX, dpiY: Double): Boolean;
  type
    TJPGHeader = array[0..1] of Byte; //FFD8 = StartOfImage (SOI)
    TJPGRecord = packed record
      Marker:  Byte;
      RecType: Byte;
      RecSize: Word;
      end;
    TAPP0Record = packed record
      JFIF:     array[0..4] of AnsiChar;  // zero-terminated "JFIF" string
      Version:  Word;       // JFIF format revision
      Units:    Byte;       // Units used for resolution: 1->inch, 2->cm, 0-> aspect ratio (1, 1)
      XDensity: Word;       // Horizontal resolution
      YDensity: Word;       // Vertical resolution
      // thumbnail follows
      end;
  var
    hdr:     TJPGHeader;
    rec:     TJPGRecord = (Marker: $FF; RecType: 0; RecSize: 0);
    app0:    TAPP0Record;
    u:       Integer;
    p:       Int64;
    n:       Integer;
    exifSig: array[0..5] of AnsiChar;
  begin
    Result := False;

    // Check for SOI (start of image) record
    n := AStream.Read(hdr{%H-}, SizeOf(hdr));
    if (n < SizeOf(hdr)) or (hdr[0] <> $FF) or (hdr[1] <> $D8) then
      Exit;

    while (AStream.Position < AStream.Size) and (rec.Marker = $FF) do
      begin
      if AStream.Read(rec, SizeOf(rec)) < SizeOf(rec) then Exit;
      rec.RecSize := BEToN(rec.RecSize);
      p           := AStream.Position - 2;
      case rec.RecType of
        $E0:  // APP0 record
          if (rec.RecSize >= SizeOf(TAPP0Record)) then
            begin
            p := AStream.Position;
            AStream.Read(app0{%H-}, SizeOf(app0));
            if stricomp(PChar(app0.JFIF), 'JFIF') <> 0 then break;
            app0.XDensity    := NToBE(Word(round(dpiX)));
            app0.YDensity    := NToBE(Word(round(dpiY)));
            app0.Units       := 1;  // 1 = per inch
            AStream.Position := p;
            AStream.WriteBuffer(app0, SizeOf(app0));
            AStream.Position := 0;
            Result           := True;
            Exit;
            end;
        $E1:   // APP1 record (EXIF)
          begin
          AStream.Read(exifSig{%H-}, Sizeof(exifSig));
               // to do: compare signature.
          Result := SetDPI_TIF(AStream, dpix, dpiY);
          if Result then
            begin
            AStream.Position := 0;
            Exit;
            end;
          end;
        end;
      AStream.Position := p + rec.RecSize;
      end;
  end;

function GetDpi_JPG(AStream: TStream; out dpiX, dpiY: Integer): Boolean;
  type
    TJPGHeader = array[0..1] of Byte; //FFD8 = StartOfImage (SOI)
    TJPGRecord = packed record
      Marker:  Byte;
      RecType: Byte;
      RecSize: Word;
      end;
    TAPP0Record = packed record
      JFIF:     array[0..4] of AnsiChar;  // zero-terminated "JFIF" string
      Version:  Word;       // JFIF format revision
      Units:    Byte;       // Units used for resolution: 1->inch, 2->cm, 0-> aspect ratio (1, 1)
      XDensity: Word;       // Horizontal resolution
      YDensity: Word;       // Vertical resolution
      // thumbnail follows
      end;
  var
    hdr:     TJPGHeader;
    rec:     TJPGRecord = (Marker: $FF; RecType: 0; RecSize: 0);
    app0:    TAPP0Record;
    u:       Integer;
    p:       Int64;
    n:       Integer;
    exifSig: array[0..5] of AnsiChar;
  begin
    Result := False;

    // Check for SOI (start of image) record
    n := AStream.Read(hdr{%H-}, SizeOf(hdr));
    if (n < SizeOf(hdr)) or (hdr[0] <> $FF) or (hdr[1] <> $D8) then
      Exit;

    while (AStream.Position < AStream.Size) and (rec.Marker = $FF) do
      begin
      if AStream.Read(rec, SizeOf(rec)) < SizeOf(rec) then Exit;
      rec.RecSize := BEToN(rec.RecSize);
      p           := AStream.Position - 2;
      case rec.RecType of
        $E0:  // APP0 record
          if (rec.RecSize >= SizeOf(TAPP0Record)) then
            begin
            p := AStream.Position;
            AStream.Read(app0{%H-}, SizeOf(app0));
            if stricomp(PChar(app0.JFIF), 'JFIF') <> 0 then break;

            dpiX   := NToBE(app0.XDensity);
            dpiY   := NToBE(app0.YDensity);
            Result := True;
            if app0.Units = 2 then  // 2 = per cm
              begin
              dpiX := round(dpiX / 2.54);
              dpiY := round(dpiY / 2.54);
              end;
            break;
            end;
        $E1:   // APP1 record (EXIF)
          begin
          AStream.Read(exifSig{%H-}, Sizeof(exifSig));
               // to do: compare signature.
          Result := GetDPI_TIF(AStream, dpiX, dpiY);
          if Result then
            begin
            AStream.Position := 0;
            Exit;
            end;
          end;
        end;
      AStream.Position := p + rec.RecSize;
      end;

    AStream.Position := 0;
  end;

procedure GetSizes_JPG(AStream: TStream; out Width, Height: Integer);
  const
    SigJPG: TBytes = ($FF, $D8);
    SigC01: TBytes = ($FF, $C0);
    SigC02: TBytes = ($FF, $C1);
  var
    Buf:                 array[0..1] of Byte;
    Offset, CheckMarker: Word;
    ImgWidth, ImgHeight: Word;
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
  function SameValue(Sig: TBytes): Boolean;
    begin
      Result := CompareMem(@Sig[0], @Buf[0], Length(Sig));
    end;
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
  function CheckMarkerOrVal(var Value: Word): Boolean;
    begin
      AStream.Read(Buf, Length(Buf));
      Value  := Swap(PWord(@Buf[0])^);
      Result := (Buf[0] = $FF);
    end;
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
  begin
    // First two bytes in a JPG file MUST be $FFD8, followed by the next marker
    if not (CheckMarkerOrVal(CheckMarker) and SameValue(SigJPG))
    then Exit;
    repeat
      if not CheckMarkerOrVal(CheckMarker)
      then Exit;
      if SameValue(SigC01) or SameValue(SigC02) then
        begin
        AStream.Position := AStream.Position + 3;
        CheckMarkerOrVal(ImgHeight);
        CheckMarkerOrVal(ImgWidth);
        Width            := ImgWidth;
        Height           := ImgHeight;
        AStream.Position := 0;
        Exit;
        end;
      CheckMarkerOrVal(Offset);
      AStream.Position := AStream.Position + Offset - 2;
    until AStream.Position > AStream.Size div 2;
    AStream.Position   := 0;
  end;


{ PNG files // https://www.w3.org/TR/PNG/ }

function ReadMWord(f: TStream): word;
  type
    TMotorolaWord = record
      case byte of
        0: (Value: word);
        1: (Byte1, Byte2: byte);
      end;
  var
    MW: TMotorolaWord;
  begin
    // It would probably be better to just read these two bytes in normally and
    // then do a small ASM routine to swap them. But we aren't talking about
    // reading entire files, so I doubt the performance gain would be worth the trouble.
    f.Read(MW.Byte2, SizeOf(Byte));
    f.Read(MW.Byte1, SizeOf(Byte));
    Result := MW.Value;
  end;

function SetDPI_PNG(AStream: TStream; dpiX, dpiY: Double): Boolean;

  type
    TPngSig   = array[0..7] of byte;
    TPngChunk = packed record
      chLength: LongInt;
      chType:   array[0..3] of AnsiChar;
      // follwoing: Data and CRC
      end;
  (*
  TPngPHYSChunk = packed record
    chType: array[0..3] of AnsiChar;
    dpiX: DWord;
    dpiY: DWord;
    unit: Byte;
  end;
   *)
  const
    ValidSig: TPNGSig = (137, 80, 78, 71, 13, 10, 26, 10);
  var
    Sig:             TPNGSig;
    x:               Integer;
    chunk:           TPngChunk;
    physChunkForCRC: array[0..12] of byte;
    xdpm:            LongInt;
    ydpm:            LongInt;
    units:           Byte;
    p, p1:           Int64;
    dw:              DWord;
    crc:             DWord;
  begin
    Result           := False;
    AStream.Position := 0;
    AStream.Read(Sig[0], SizeOf(Sig));
    for x := Low(Sig) to High(Sig) do
      if Sig[x] <> ValidSig[x] then
        Exit;
    AStream.Position := SizeOf(TPngSig);
    while AStream.Position < AStream.Size do
      begin
      AStream.Read(chunk, SizeOf(TPngChunk));
      chunk.chLength := BEToN(chunk.chLength);
      p              := AStream.Position;
      if strlcomp(PChar(chunk.chType), 'pHYs', 4) = 0 then
        begin
        dw               := Round(dpix / 0.0254); // pixels per meter
        dw               := NToBE(DWord(dw));
        AStream.WriteDWord(dw);
        dw               := Round(dpiy / 0.0254);
        dw               := NToBE(DWord(dw));
        AStream.WriteDWord(dw);
        AStream.WriteByte(1);  // Unit: per meter
        p1               := AStream.Position;
        AStream.Position := p - 4;
        AStream.ReadBuffer(physChunkForCRC[0], Length(physChunkForCRC));
        crc              := CalculateCRC(physChunkForCRC[0], Length(physChunkForCRC));
        crc              := NToBE(DWord(crc));
        AStream.Position := p1;
        AStream.WriteDWord(crc);
        AStream.Position := 0;
        Result           := True;
        Exit;
        end;
      AStream.Position := p + chunk.chLength + 4;
      end;
  end;

function GetDPI_PNG(AStream: TStream; out dpiX, dpiY: Integer): Boolean;
    // https://www.w3.org/TR/PNG/
  type
    TPngSig   = array[0..7] of byte;
    TPngChunk = packed record
      chLength: LongInt;
      chType:   array[0..3] of AnsiChar;
      // follwoing: Data and CRC
      end;
  (*
  TPngPHYSChunk = packed record
    chType: array[0..3] of AnsiChar;
    dpiX: DWord;
    dpiY: DWord;
    unit: Byte;
  end;
   *)
  const
    ValidSig: TPNGSig = (137, 80, 78, 71, 13, 10, 26, 10);
  var
    Sig:             TPNGSig;
    x:               Integer;
    chunk:           TPngChunk;
    physChunkForCRC: array[0..12] of byte;
    xdpm:            LongInt;
    ydpm:            LongInt;
    units:           Byte;
    p, p1:           Int64;
    dw:              DWord;
    crc:             DWord;
  begin
    Result           := False;
    AStream.Position := 0;
    AStream.Read(Sig[0], SizeOf(Sig));
    for x := Low(Sig) to High(Sig) do
      if Sig[x] <> ValidSig[x] then
        Exit;
    AStream.Position := SizeOf(TPngSig);
    while AStream.Position < AStream.Size do
      begin
      AStream.Read(chunk, SizeOf(TPngChunk));
      chunk.chLength := BEToN(chunk.chLength);
      p              := AStream.Position;
      if strlcomp(PChar(chunk.chType), 'pHYs', 4) = 0 then
        begin
        dpiX := Round(NToBE(AStream.ReadDWord{pixels per meter}) * 0.0254);
        dpiY := Round(NToBE(AStream.ReadDWord{pixels per meter}) * 0.0254);
        if AStream.ReadByte <> 1 then   // 1 = Unit: per meter
          begin
          dpiX := 0;
          dpiY := 0;
          end;
        Result := True;
        Exit;
        end;
      AStream.Position := p + chunk.chLength + 4;
      end;
    AStream.Position   := 0;
  end;

procedure GetSizesPNG(AStream: TStream; out Width, Height: Integer);
  type
    TPNGSig = array[0..7] of byte;
  const
    ValidSig: TPNGSig = (137, 80, 78, 71, 13, 10, 26, 10);
  var
    Sig: TPNGSig;
    x:   Integer;
  begin
    FillChar(Sig, SizeOf(Sig), #0);

    AStream.Read(Sig[0], SizeOf(Sig));
    for x := Low(Sig) to High(Sig) do
      if Sig[x] <> ValidSig[x] then
        Exit;
    AStream.Seek(18, 0);
    Width            := ReadMWord(AStream);
    AStream.Seek(22, 0);
    Height           := ReadMWord(AStream);
    AStream.Position := 0;
  end;


{ PCX files }

function SetDPI_PCX(AStream: TStream; dpiX, dpiY: Double): Boolean;
  type
    TPCXHeader = packed record
      FileID:       Byte;                      // $0A for PCX files, $CD for SCR files
      Version:      Byte;                     // 0: version 2.5; 2: 2.8 with palette; 3: 2.8 w/o palette; 5: version 3
      Encoding:     Byte;                      // 0: uncompressed; 1: RLE encoded
      BitsPerPixel: Byte;
      XMin,
      YMin,
      XMax,
      YMax,                                      // coordinates of the corners of the image
      HRes,                                      // horizontal resolution in dpi
      VRes:         Word;                        // vertical resolution in dpi
      ColorMap:     array[0..15 * 3] of byte;    // color table
      Reserved,
      ColorPlanes:  Byte;                        // color planes (at most 4)
      BytesPerLine,                              // number of bytes of one line of one plane
      PaletteType:  Word;                        // 1: color or b&w; 2: gray scale
      Fill:         array[0..57] of Byte;
      end;
  var
    hdr: TPCXHeader;
    n:   Int64;
  begin
    Result := False;

    AStream.Position := 0;
    n                := AStream.Read(hdr{%H-}, SizeOf(hdr));
    if n < SizeOf(hdr) then Exit;
    if not (hdr.FileID in [$0A, $CD]) then Exit;

    hdr.HRes         := round(dpix);
    hdr.VRes         := round(dpiy);
    AStream.Position := 0;
    AStream.WriteBuffer(hdr, Sizeof(hdr));
    AStream.Position := 0;
    Result           := True;
  end;


{ WMF files }

function SetDPI_WMF(AStream: TStream; dpi: Double): Boolean;
  type
    TWMFSpecialHeader = packed record
      Key:      DWord;       // Magic number (always $9AC6CDD7)
      Handle:   Word;        // Metafile HANDLE number (always 0)
      Left:     SmallInt;    // Left coordinate in metafile units (twips)
      Top:      SmallInt;    // Top coordinate in metafile units
      Right:    SmallInt;    // Right coordinate in metafile units
      Bottom:   SmallInt;    // Bottom coordinate in metafile units
      Inch:     Word;        // Number of metafile units per inch
      Reserved: DWord;       // Reserved (always 0)
      Checksum: Word;        // Checksum value for previous 10 words
      end;
  var
    hdr: TWMFSpecialHeader;
    n:   Int64;
  begin
    Result := False;

    AStream.Position := 0;
    n                := AStream.Read(hdr{%H-}, SizeOf(hdr));
    if n < SizeOf(hdr) then Exit;
    if hdr.Key <> $9AC6CDD7 then Exit;

    hdr.Inch         := Round(dpi);
    AStream.Position := 0;
    AStream.WriteBuffer(hdr, SizeOf(hdr));
    AStream.Position := 0;
    Result           := True;
  end;


procedure GetGIFSize(var AFilename: String; var Width, Height: Word);
  var
    HeaderStr: AnsiString;
    Stream:    TMemoryStream;

  begin
    Stream := TMemoryStream.Create;
    Stream.LoadFromFile(AFilename);

    //Result := False;
    Width  := 0;
    Height := 0;

    //GIF header is 13 bytes in length
    if Stream.Size > 13 then
      begin
      SetString(HeaderStr, PAnsiChar(Stream.Memory), 6);
      if (HeaderStr = 'GIF89a') or (HeaderStr = 'GIF87a') then
        begin
        Stream.Seek(6, soFromBeginning);
        Stream.Read(Width, 2);  //Width is located at bytes 7-8
        Stream.Read(Height, 2); //Height is located at bytes 9-10

        //Result := True;
        end;
      end;

    FreeAndNil(Stream);
  end;


function SetImageDPI(AFilename: String; ANewDPI: Integer): Boolean;
  var
    stream: TFileStream;
  begin
    Result := False;
    stream := TFileStream.Create(AFilename, fmOpenReadWrite);
      try
      case UTF8LowerCase(ExtractFileExt(AFilename)) of
        '.png':
          Result := SetDPI_PNG(stream, ANewDPI, ANewDPI);

        '.bmp':
          Result := SetDpi_BMP(stream, ANewDPI, ANewDPI);

        '.jpg', '.jpeg':
          Result := SetDpi_JPG(stream, ANewDPI, ANewDPI);

        '.tif', '.tiff':
          Result := SetDPI_TIF(stream, ANewDPI, ANewDPI);
        end;
      finally
      FreeAndNil(stream);
      end;
  end;

end.
