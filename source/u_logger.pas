unit u_logger;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SynEdit;

resourcestring
  TXT_LOG_START = 'Лог начат';

type

  { TCustomLogger }

  TCustomLogger = class
  private
    FFilename:    String;
    FLineEnding:  String;
    FSynEdit:     TSynEdit;
    FWriteToFile: Boolean;

    procedure AddToSynEdit(AData: String);
    procedure AddToFile(AData: String);
    procedure SetFilename(AValue: String);

  public
    constructor Create(ALineEnding: String = LineEnding);
    destructor Destroy; override;

    function Add(AData: String): String;
    function AddFmt(const AData: String; const Args: array of const): String;

  public
    property WriteToFile: Boolean read FWriteToFile write FWriteToFile;
    property Filename: String read FFilename write SetFilename;
    property SynEdit: TSynEdit read FSynEdit write FSynEdit;
  end;

implementation

{ TCustomLogger }

procedure TCustomLogger.AddToSynEdit(AData: String);
  begin
    if FSynEdit = nil then Exit;
    FSynEdit.Text := FSynEdit.Text + AData;

    // прокрутка в конец
    FSynEdit.TopLine := FSynEdit.Lines.Count;
    FSynEdit.CaretY  := FSynEdit.Lines.Count;
  end;

procedure TCustomLogger.AddToFile(AData: String);
  var
    f: TextFile;
  begin
    if (FFilename = '') or not FWriteToFile then Exit;

      try
      AssignFile(f, FFilename);

      if FileExists(FFilename) then
        Append(f)
      else
        Rewrite(f);

      Write(f, AData);
      finally
      CloseFile(f);
      end;
  end;

function TCustomLogger.Add(AData: String): String;
  begin
    AddToFile(AData + FLineEnding);
    AddToSynEdit(AData + FLineEnding);
    Result := AData;
  end;

function TCustomLogger.AddFmt(const AData: String;
  const Args: array of const): String;
  begin
    Result   := '';
      try
      Result := Add(UnicodeFormat(AData, Args));
      except
      end;
  end;

procedure TCustomLogger.SetFilename(AValue: String);
  begin
    if FFilename = AValue then Exit;
    FFilename := AValue;

    if FileExists(FFilename) then
      AddToFile(LineEnding + LineEnding);

    AddToFile(TXT_LOG_START + ' ' + DateTimeToStr(Now) + LineEnding + LineEnding);
  end;

constructor TCustomLogger.Create(ALineEnding: String);
  begin
    FLineEnding  := ALineEnding;
    FFilename    := '';
    FSynEdit     := nil;
    FWriteToFile := False;
  end;

destructor TCustomLogger.Destroy;
  begin
    inherited Destroy;
  end;

end.
